<?php 

class CacheRevalidationAPI {
    private $url;
    private $apiKey;
    private $compareKey;
    private $path;

    public function __construct() {
        add_action('rest_api_init', [$this, 'register_routes']);
    }

    // Register REST API routes
    public function register_routes() {
        // Route to clear cache and revalidate
        register_rest_route('seraph_accel/v1', '/clear_cache', [
            'methods' => ['POST', 'GET'],
            'callback' => [$this, 'handle_cache_request'],
        ]);

        // Route to get cache status
        register_rest_route('seraph_accel/v1', '/get_cache_status', [
            'methods' => ['POST', 'GET'],
            'callback' => [$this, 'handle_cache_status_request'],
        ]);
    }

    // Common function to validate URL, API keys, and home URL check
    private function validate_request_params(WP_REST_Request $request) {
        $request_body = $request->get_json_params(); // Get the JSON body

        $this->url = isset($request_body['url']) ? esc_url_raw($request_body['url']) : '';
        $this->apiKey = isset($request_body['apiKey']) ? sanitize_text_field($request_body['apiKey']) : '';
        $this->compareKey = isset($request_body['compareKey']) ? sanitize_text_field($request_body['compareKey']) : '';
        // $this->path = isset($request_body['path']) ? sanitize_text_field($request_body['path']) : '/';
        $this->path = $request->get_param('path') 
    ? sanitize_text_field($request->get_param('path')) 
    : (isset($request_body['path']) && !empty($request_body['path']) 
        ? sanitize_text_field($request_body['path']) 
        : '/');

        // Check if URL matches home URL
        if (home_url() !== $this->url) {
            return new WP_REST_Response(['error' => 'URL does not match the site\'s home URL.'], 403);
        }

        // Validate the user data with the external API
        $permission_check = $this->get_permission_by_api();

        if ($permission_check !== true) {
            return $permission_check; // This will return the WP_REST_Response with an error.
        }

        return true; // All validations passed
    }

    // Handle the cache revalidation request
    public function handle_cache_request(WP_REST_Request $request) {
        $validation_result = $this->validate_request_params($request);
        if ($validation_result !== true) {
            return $validation_result; // Return the validation error response
        }

        // Check if \seraph_accel\API class exists
        if (!class_exists('\seraph_accel\API')) {
            return new WP_REST_Response(['error' => 'Cache API class does not exist'], 500);
        }

        // Revalidate cache and check status
        \seraph_accel\API::OperateCache(\seraph_accel\API::CACHE_OP_CHECK_REVALIDATE, $this->path);
        $page_status = \seraph_accel\API::GetCacheStatus($this->url . $this->path);

        // Revalidate if cache status is not valid
        if ($this->is_revalidation_required($page_status)) {
            \seraph_accel\API::OperateCache(\seraph_accel\API::CACHE_OP_CHECK_REVALIDATE, $this->path);
            $status_message = 'Revalidated again due to failed initial status';
        } else {
            $status_message = 'Revalidated successfully';
        }

        return new WP_REST_Response([
            'message' => $status_message,
            'path' => $this->path,
            'page_status' => $page_status
        ], 200);
    }

    // Handle the get cache status request
    public function handle_cache_status_request(WP_REST_Request $request) {
        $validation_result = $this->validate_request_params($request);
        if ($validation_result !== true) {
            return $validation_result; // Return the validation error response
        }

        // Check if \seraph_accel\API class exists
        if (!class_exists('\seraph_accel\API')) {
            return new WP_REST_Response(['error' => 'Cache API class does not exist'], 500);
        }

        // Get the cache status
        $cache_status = \seraph_accel\API::GetCacheStatus($this->url . $this->path);

        return new WP_REST_Response([
            'status' => $cache_status,
            'path' => $this->path
        ], 200);
    }

    // Request third-party API for permission
    private function get_permission_by_api() {
        $api_url = 'https://app.agencyframework.io/api/web-utility/slack/auth/compare';

        // Prepare data for the API request
        $body = [
            'apiKey' => $this->apiKey,
            'url' => $this->url,
            'compareKey' => $this->compareKey,
        ];

        // Send the API request
        $response = wp_remote_post($api_url, [
            'body' => json_encode($body),
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);

        // Check for errors
        if (is_wp_error($response)) {
            return new WP_REST_Response(['error' => 'Error while connecting to the external API.'], 500);
        }

        // Retrieve response data
        $response_body = wp_remote_retrieve_body($response);
        $response_data = json_decode($response_body, true);

        // Handle API error responses
        if (isset($response_data['status']) && !$response_data['status']) {
            return new WP_REST_Response(['error' => $response_data['message'] ?? 'Unknown error'], 403);
        }

        // If successful, return true
        if (isset($response_data['status']) && $response_data['status'] === true) {
            return true;
        }

        return new WP_REST_Response(['error' => 'API response did not match expected fields.'], 500);
    }

    // Determine if revalidation is required based on cache status
    private function is_revalidation_required($page_status) {
        return ($page_status['cache'] === false || $page_status['optimization'] === NULL || $page_status['status'] === 'none');
    }
}

// Initialize the CacheRevalidationAPI class
new CacheRevalidationAPI();

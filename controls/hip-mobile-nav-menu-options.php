<?php

class Hip_Mobile_Nav_Menu_Options extends \Elementor\Widget_Base {

    public function __construct() {
        parent::__construct();
        $this->init_control();
    }

    public function get_name() {
        return 'hip-nav-menu';
    }

    public function hip_register_controls( $element, $args ) {

        $element->start_controls_section(
            'hip-nav-addon-section',
            [
                'label' => __( 'HIP Mobile 2nd Level Menu Options', 'hip' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $element->add_control(
            'hip-menu-border-width',
            [
                'label' => __( 'Menu Border Width', 'hip' ),
                'devices' => [ 'tablet', 'mobile' ],
                'type' => \Elementor\Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                    ],
                ],
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul.elementor-nav-menu li:not(.border-0)' => 'border-bottom-width: {{SIZE}}{{UNIT}}; border-bottom-style: solid;',
                    '(tablet) {{WRAPPER}} ul.elementor-nav-menu li:last-child' => 'border-bottom: none;'
                ],

            ]
        );

        $element->add_control(
            'hip-main-menu-color',
            [
                'label' => __( 'Main menu border Color Only', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul.elementor-nav-menu > li' => 'border-bottom-color: {{VALUE}}!important; ',
                ],
            ]
        );


        $element->add_control(
            'hip-menu-border-color',
            [
                'label' => __( 'Child Menu Border & other border Color', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul.elementor-nav-menu li' => 'border-bottom-color: {{VALUE}}; border-bottom-style: solid;',
                    '(tablet) {{WRAPPER}} ul.elementor-nav-menu li:last-child' => 'border-bottom: none;',
                ],

            ]
        );

        $element->add_control(
            'hip-child-menu-bgcolor',
            [
                'label' => __( 'Child Menu Background', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul.elementor-nav-menu--dropdown li a' => 'background-color: {{VALUE}} !important;',
                ],
                'separator' => 'before',

            ]
        );

        $element->add_control(
            'hip-child-menu-bgcolor-hover',
            [
                'label' => __( 'Child Menu Background on Hover', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul.elementor-nav-menu--dropdown li a:hover' => 'background-color: {{VALUE}} !important;',
                    '(tablet) {{WRAPPER}} ul.elementor-nav-menu--dropdown li a.elementor-item-active' => 'background-color: {{VALUE}} !important;'
                ],

            ]
        );

        $element->add_control(
            'hip-child-menu-txcolor',
            [
                'label' => __( 'Child Menu Text Color', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul.elementor-nav-menu--dropdown li a' => 'color: {{VALUE}} !important;'
                ],
                'separator' => 'before',
            ]
        );

        $element->add_control(
            'hip-child-menu-txcolor-hover',
            [
                'label' => __( 'Child Menu Text Color on Hover', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul.elementor-nav-menu--dropdown li a:hover' => 'color: {{VALUE}} !important;',
                    '(tablet) {{WRAPPER}} ul.elementor-nav-menu--dropdown li a.elementor-item-active' => 'color: {{VALUE}} !important;',
                ],

            ]
        );






        $element->add_control(
            'hip-menu-fullscreen',
            [
                'label' => __( 'Menu FullScreen (Yes/No)', 'hip' ),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'default' => '',
                'label_on' => 'Yes',
                'label_off' => 'No',
                'return_value' => 'yes',
                'selectors' => [
                    '(mobile) {{WRAPPER}} nav.elementor-nav-menu__container' => 'height: 100vh; max-height: 90vh;',
                    '(tablet) {{WRAPPER}} nav.elementor-nav-menu__container' => 'height: 100vh; max-height: 90vh;',
                ],
                'separator' => 'before',
            ]
        );

        $element->end_controls_section();


        $this->hip_2nd_level_optional_menu( $element, $args);
        $this->hip_3rd_level_menu( $element, $args);
        $this->hip_menu_general_setting( $element, $args);
    }

    protected  function hip_2nd_level_optional_menu($element, $args){
        $element->start_controls_section(
            'hip-nav-2ndmega-section',
            [
                'label' => __( 'HIP Mobile 2nd level Mega Menu Options (Optional)', 'hip' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $element->add_control(
            'hip-menu-2nopt-border-width',
            [
                'label' => __( 'Menu Border Width', 'hip' ),
                'devices' => [ 'tablet', 'mobile' ],
                'type' => \Elementor\Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} ul  li.mobile-megamenu ul.elementor-nav-menu--dropdown  li' => 'border-bottom-width: {{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} ul li.mobile-megamenu ul.elementor-nav-menu--dropdown  li:last-child' => 'border-bottom: none;'
                ],
                'separator' => 'before',
            ]
        );


        $element->add_control(
            'hip-menu-2ndmega-border-color',
            [
                'label' => __( 'Menu Border Color', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} ul > li.mobile-megamenu  ul.elementor-nav-menu--dropdown  li' => 'border-bottom-color: {{VALUE}};',
                    '{{WRAPPER}} ul > li.mobile-megamenu  ul.elementor-nav-menu--dropdown  li:last-child' => 'border-bottom: none;'
                ],
            ]
        );

        $element->add_control(
            'hip-child-2ndmega-bgcolor',
            [
                'label' => __( 'Child Menu Background', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul  li.mobile-megamenu ul.elementor-nav-menu--dropdown  li a' => 'background-color: {{VALUE}} !important;',
                ],
                'separator' => 'before',
            ]
        );

        $element->add_control(
            'hip-child-2ndmega-bgcolor-hover',
            [
                'label' => __( 'Child Menu Background on Hover', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul  li.mobile-megamenu ul.elementor-nav-menu--dropdown  li a.elementor-item-active' => 'background: {{VALUE}} !important;',
                    '(tablet) {{WRAPPER}} ul  li.mobile-megamenu ul.elementor-nav-menu--dropdown  li a.highlighted' => 'background: {{VALUE}} !important;',
                    '(tablet) {{WRAPPER}} ul  li.mobile-megamenu ul.elementor-nav-menu--dropdown  li a:hover' => 'background-color: {{VALUE}} !important;',
                ],
            ]
        );

        $element->add_control(
            'hip-child-2ndmega-txcolor',
            [
                'label' => __( 'Child Menu Text Color', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul  li.mobile-megamenu ul.elementor-nav-menu--dropdown  li a' => 'color: {{VALUE}} !important;',
                ],
                'separator' => 'before',
            ]
        );

        $element->add_control(
            'hip-child-2ndmega-txcolor-hover',
            [
                'label' => __( 'Child Menu Text Color on Hover', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul  li.mobile-megamenu ul.elementor-nav-menu--dropdown  li a:hover' => 'color: {{VALUE}} !important;',
                    '(tablet) {{WRAPPER}} ul  li.mobile-megamenu ul.elementor-nav-menu--dropdown  li a.elementor-item-active' => 'color: {{VALUE}} !important;',
                ],
            ]
        );


        ;

        $element->end_controls_section();
    }

    protected  function hip_3rd_level_menu($element, $args){
        $element->start_controls_section(
            'hip-nav-3rdmega-section',
            [
                'label' => __( 'HIP Mobile 3rd level Menu Options', 'hip' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $element->add_control(
            'hip-menu-3rdmega-border-width',
            [
                'label' => __( 'Menu Border Width', 'hip' ),
                'devices' => [ 'tablet', 'mobile' ],
                'type' => \Elementor\Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                    ],
                ],
                'selectors' => [
                    '(tablet)  {{WRAPPER}} ul > li:nth-child(2) ul.elementor-nav-menu--dropdown li li' => 'border-bottom-width: {{SIZE}}{{UNIT}};',
                    '(tablet) {{WRAPPER}} ul > li:nth-child(2) ul.elementor-nav-menu--dropdown li li:last-child' => 'border-bottom: none;',
                ],
                'separator' => 'before',
            ]
        );


        $element->add_control(
            'hip-menu-3rdmega-border-color',
            [
                'label' => __( 'Menu Border Color', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul > li:nth-child(2) ul.elementor-nav-menu--dropdown li li' => 'border-bottom-color: {{VALUE}};',
                    '(tablet) {{WRAPPER}} ul > li:nth-child(2) ul.elementor-nav-menu--dropdown li li:last-child' => 'border-bottom: none;'
                ],
            ]
        );

        $element->add_control(
            'hip-child-3rdmega-bgcolor',
            [
                'label' => __( 'Child Menu Background', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) {{WRAPPER}} ul > li:nth-child(2) ul.elementor-nav-menu--dropdown li li a' => 'background-color: {{VALUE}} !important;',
                ],
                'separator' => 'before',
            ]
        );

        $element->add_control(
            'hip-child-3rdmega-bgcolor-hover',
            [
                'label' => __( 'Child Menu Background on Hover', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet)  {{WRAPPER}} ul > li:nth-child(2) ul.elementor-nav-menu--dropdown li li a:hover' => 'background-color: {{VALUE}} !important;',
                    '(tablet)  {{WRAPPER}} ul > li:nth-child(2) ul.elementor-nav-menu--dropdown > li ul > li a.elementor-item-active' => 'background: {{VALUE}} !important;',
                ],
            ]
        );

        $element->add_control(
            'hip-child-3rdmega-txcolor',
            [
                'label' => __( 'Child Menu Text Color', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet)  {{WRAPPER}} ul > li:nth-child(2) ul.elementor-nav-menu--dropdown li li a' => 'color: {{VALUE}} !important;',
                ],
                'separator' => 'before',
            ]
        );

        $element->add_control(
            'hip-child-3rdmega-txcolor-hover',
            [
                'label' => __( 'Child Menu Text Color on Hover', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet)  {{WRAPPER}} ul > li:nth-child(2) ul.elementor-nav-menu--dropdown li li a:hover' => 'color: {{VALUE}} !important;',
                    '(tablet)  {{WRAPPER}} ul > li:nth-child(2) ul.elementor-nav-menu--dropdown > li ul > li a.elementor-item-active' => 'color: {{VALUE}} !important;',
                ],
            ]
        );

        $element->end_controls_section();
    }

    protected  function hip_menu_general_setting($element, $args){
        $element->start_controls_section(
            'hip-nav-settings-section',
            [
                'label' => __( 'HIP Mobile Settings ', 'hip' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );




        $element->add_control(
            'hip-toogle-iconbg-color',
            [
                'label' => __( 'Toogle Bar  Color', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '.mobile-main-header .elementor-menu-toggle .eicon-menu-bar,
					.mobile-main-header .elementor-menu-toggle .eicon-menu-bar::after,
					.mobile-main-header .elementor-menu-toggle .eicon-menu-bar::before,
					.mobile-main-header .elementor-menu-toggle.elementor-active .eicon-menu-bar::before,
					.mobile-main-header .elementor-menu-toggle.elementor-active .eicon-menu-bar::after' => 'background: {{VALUE}} !important;',
                ],
                'separator' => 'before',
            ]
        );

        $element->add_control(
            'hip-sticky-toogle-iconbg-color',
            [
                'label' => __( 'Fixed Toogle Bar Color', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '.fixed-header.mobile-main-header .elementor-menu-toggle .eicon-menu-bar::after,
					.fixed-header.mobile-main-header .elementor-menu-toggle .eicon-menu-bar::before,
					.fixed-header.mobile-main-header .elementor-menu-toggle .eicon-menu-bar,
					.fixed-header.mobile-main-header .elementor-menu-toggle.elementor-active .eicon-menu-bar::before,
					.fixed-header.mobile-main-header .elementor-menu-toggle.elementor-active .eicon-menu-bar::after' => 'background: {{VALUE}} !important;',
                ],
            ]
        );

        $element->add_control(
            'hip-fixed-mobile-icon-border',
            [
                'label' => __( 'Fixed Mobile Icon Border Color', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '.fixed-header.mobile-main-header .elementor-icon-list-items a svg path:first-child' => 'fill: {{VALUE}} !important;',
                ],
                'separator' => 'before',
            ]
        );

        $element->add_control(
            'hip-fixed-mobile-icon-color',
            [
                'label' => __( 'Fixed Mobile Icon Color', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '.fixed-header.mobile-main-header .elementor-icon-list-items a svg path:last-child' => 'fill: {{VALUE}} !important;',
                ],
            ]
        );

        $element->add_control(
            'hip-fixed-header-bg-clr',
            [
                'label' => __( 'Fixed Header Background tablet', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(tablet) .elementor-location-header .mobile-main-header.fixed-header' => 'background-color: {{VALUE}}!important;',
                ],
                'separator' => 'before',
            ]
        );
        $element->add_control(
            'hip-fixed-header-bg-mobile-clr',
            [
                'label' => __( 'Fixed Header Background Mobile', 'hip' ),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '(mobile) .elementor-location-header .mobile-main-header.fixed-header' => 'background-color: {{VALUE}}!important;',
                ],
                'separator' => 'before',
            ]
        );



        $element->end_controls_section();
    }

    protected function init_control() {
        add_action( 'elementor/element/nav-menu/style_toggle/after_section_end', [ $this, 'hip_register_controls' ], 10, 2 );
    }

}

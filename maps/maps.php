<?php
namespace Elementor;

use HipAddons\Admin\Includes\Admin_Helper;
use function ReactWPScripts\get_plugin_basedir_path;

if (! defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Hip_Maps_Elementor_Widget extends Widget_Base
{

    /**
     * construct
     */
    public function __construct($data = [], $args = null)
    {
        parent::__construct($data, $args);
        //add_action('elementor/frontend/after_register_styles', [$this,'hip_team_style']);
        
            wp_register_style('hip-map-widget-css', plugins_url('/assets/css/leaflet.css', __FILE__));
            wp_register_style('hip-map-widget-custom-css', plugins_url('/assets/css/hip-map-style.css', __FILE__));    

    }

    public function get_style_depends()
    {
        return ['hip-map-widget-css', 'hip-map-widget-custom-css'];
    }


//
//    public function get_script_depends()
//    {
//        return array(
//            'hip-leaflet-js',
//            'hip-leaflet-provider',
//            'hip-custom-map',
//
//        );
//    }

    /**
     * Retrieve Widget Support URL.
     *
     * @access public
     *
     * @return string support URL.
     */
    public function get_custom_help_url()
    {
        return 'https://premiumaddons.com/support/';
    }

    public static function getLocations()
    {
        $posts = get_posts(
            array(
                'post_type' => 'location',
                'posts_per_page'=> -1,
            )
        );
        $locations = array();
        foreach ($posts as  $post) {
            array_push(
                $locations,
                array(
                    'hip_map_latitude'  => $post->location_lat,
                    'hip_map_longitude' => $post->location_lng,
                    'hip_pin_title'     => $post->post_title,
                )
            );

        }

        return $locations;

    }

	public static function getLocationsUrl()
	{
		$posts = get_posts(
            array(
                'post_type' => 'location',
                'posts_per_page' => -1,
            )
        );
		$locations_url = [];
        foreach ($posts as  $key=>$post) {
            $only_path = preg_replace('#^.+://[^/]+#', '', get_the_permalink($post->ID));
            $locations_url[$only_path] = $post->post_title;
        };

        return $locations_url;
    }


    /**
     * Get widget name
     * @return string
     */
    public function get_name()
    {

        return 'hip-map-id';
    }

    /**
     * Get widget title
     * @return string
     */
    public function get_title()
    {
        return esc_html('Hip Maps ', 'hip');
    }

    /**
     * Get widget icon
     * @return string|void
     */
    public function get_icon()
    {
        return 'eicon-map-pin';
    }

    /**
     * Get widget category
     * @return array
     */

    public function get_categories()
    {
        return ['hip-addons'];
    }
    /**
     * Widget preview refresh button.
     *
     * @since 1.0.0
     * @access public
     */
    public function is_reload_preview_required()
    {
        return true;
    }
    /**
     * Register widget controls
     */
    protected function register_controls()
    {
        $this->start_controls_section(
            'hip_maps_map_settings',
            array(
                'label' => __('Center Location', 'hip'),
            )
        );



        $this->add_control(
            'hip_maps_center_lat',
            array(
                'label'       => __('Center Latitude', 'hip'),
                'type'        => Controls_Manager::TEXT,
                'dynamic'     => array('active' => true),
                'description' => __('Center latitude and longitude are required to identify your location', 'hip'),
                'default'     => '30.4160534',
                'label_block' => true,

            )
        );

        $this->add_control(
            'hip_maps_center_long',
            array(
                'label'       => __('Center Longitude', 'hip'),
                'type'        => Controls_Manager::TEXT,
                'dynamic'     => array('active' => true),
                'description' => __('Center latitude and longitude are required to identify your location', 'hip'),
                'default'     => '-87.2226216',
                'label_block' => true,

            )
        );

        $this->end_controls_section();
        $this->map_marker_control();
        $this->map_settings_controls();
        $this->style_content_register_controls();
        $this->style_close_icon_register_controls();
        $this->style_map_box_register_controls();
    }
    //map marker controls
    protected function map_marker_control(){
        $this->start_controls_section(
            'hip_maps_pins_settings',
            array(
                'label' => __('Markers', 'hip'),

            )
        );

        $this->add_control(
            'generate_locations',
            array(
                'label' => 'Generate Locations',
                'type'  => Controls_Manager::SWITCHER,
                'return_value' => 'true',

            )
        );

        $repeater = new Repeater();

        $repeater->add_control(
            'hip_pin_icon',
            array(
                'label'   => __('Custom Icon', 'hip'),
                'type'    => Controls_Manager::MEDIA,
                'dynamic' => array('active' => true),
                'default' =>[
                    'url' => plugin_dir_url(dirname(__FILE__)).'maps/assets/image/pin-icon.svg',
                ]
            )
        );

        $repeater->add_control(
            'hip_map_latitude',
            array(
                'name'        => 'hip_map_latitude',
                'label'       => __('Latitude', 'hip'),
                'type'        => Controls_Manager::TEXT,
                'dynamic'     => array('active' => true),
                'description' => 'Click <a href="https://www.latlong.net/" target="_blank">here</a> to get your location coordinates',
                'label_block' => true,
                'default'=>'30.41605344'
            )
        );

        $repeater->add_control(
            'hip_map_longitude',
            array(
                'name'        => 'hip_map_longitude',
                'label'       => __('Longitude', 'hip'),
                'type'        => Controls_Manager::TEXT,
                'dynamic'     => array('active' => true),
                'description' => 'Click <a href="https://www.latlong.net/" target="_blank">here</a> to get your location coordinates',
                'label_block' => true,
                'default' => '-87.2226216'
            )
        );

        $repeater->add_control(
            'hip_pin_title',
            array(
                'label'       => __('Title', 'hip'),
                'type'        => Controls_Manager::WYSIWYG,
                'dynamic'     => array('active' => true),
                'label_block' => true,
            )
        );

        $repeater->add_control(
            'hip_pin_desc',
            array(
                'label'       => __('Select Location URL', 'hip'),
                'type'        => Controls_Manager::SELECT,
                'dynamic'     => array('active' => true),
                'label_block' => true,
                'options' => Hip_Maps_Elementor_Widget::getLocationsUrl(),
            )
        );





        $repeater->add_control(
            'hip_desc_color',
            [
                'label' => esc_html__( 'Text Color', 'hip' ),
                'type' =>Controls_Manager::COLOR,
                'default'	=> '#000',
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}  a' => 'color: {{VALUE}}',
                ],
                'separator' => 'before',
            ]

        );

        $repeater->add_control(
            'hip_desc_hover_color',
            [
                'label' => esc_html__( 'Text hover Color', 'hip' ),
                'type' => Controls_Manager::COLOR,
                'default'	=> '#fff',
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}  .leaflet-popup-content-wrapper:hover .leaflet-popup-content a' => 'color: {{VALUE}}',
                ],
            ]
        );

        $repeater->add_control(
            'hip_box_bgclr',
            [
                'label' => esc_html__( 'Info Box BG', 'hip' ),
                'type' => Controls_Manager::COLOR,
                'default'	=> '#1EB2F1',
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}} .leaflet-popup-content-wrapper' => 'background: {{VALUE}};',
                    '{{WRAPPER}} {{CURRENT_ITEM}} .leaflet-popup-tip' => 'border-top-color: {{VALUE}};',
                ],
            ]
        );

		$repeater->add_control(
				'hip_box_bgclr_hover',
				[
                    'label' => esc_html__( 'Info Box BG Hover', 'hip' ),
                    'type' => Controls_Manager::COLOR,
                    'default'	=> '#000',
                    'selectors' => [
                            '{{WRAPPER}} {{CURRENT_ITEM}} .leaflet-popup-content-wrapper:hover' => 'background: {{VALUE}};',
                        '{{WRAPPER}} {{CURRENT_ITEM}} .leaflet-popup-content-wrapper:hover + .leaflet-popup-tip-container .leaflet-popup-tip' => 'border-top-color: {{VALUE}}'
                    ],
				]
		);


        $this->add_control(
            'hip_maps_map_pins',
            array(
                'label'       => __('Map Pins', 'hip'),
                'type'        => Controls_Manager::REPEATER,
                'default'     => array(
                    'hip_map_latitude'  => '30.4160535',
                    'hip_map_longitude' => '-87.2226216',
                    'hip_pin_title'     => __('Hip custom  Maps', 'hip'),
                    'hip_pin_desc'      => __('Add an optional description to your map pin', 'hip'),
                ),
                'fields'      => $repeater->get_controls(),
                'title_field' => '{{{ hip_pin_title }}}',
                'prevent_empty' => false,
                'separator' => 'before',
            )
        );


        $this->add_control(
            'cpt_maps_location_map_pins',
            array(
                'label'       => __('Location Map Pins', 'hip'),
                'type'        => Controls_Manager::REPEATER,
                'default'     => Hip_Maps_Elementor_Widget::getLocations(),
                'fields'      => $repeater->get_controls(),
                'title_field' => '{{{ hip_pin_title }}}',
                'prevent_empty' => false,
                'condition' => array(
                    'generate_locations' => 'true',
                ),
            )
        );


        $this->end_controls_section();
    }
    //map settings controls
    protected function map_settings_controls(){
        //controls
        $this->start_controls_section(
            'map-settings-section',
            [
                'label'=>__('Settings', 'hip'),
                'tab'=> \Elementor\Controls_Manager::TAB_CONTENT
            ]
        );

        $this->add_control(
            'hip_map_fit_bounds',
            [
                'label' => __('Auto Fit Map Bounds', 'hip'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('On', 'hip'),
                'label_off' => __('Off', 'hip'),
                'default' => 'yes',
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'hip_map_zoom_control',
            [
                'label' => __('Zoom Control Option', 'hip'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Show', 'hip'),
                'label_off' => __('Hide', 'hip'),
                'default' => 'no',
                'separator' => 'before',
            ]
        );


        $this->add_control(
            'hip_map_dbl_click_option',
            [
                'label' => __('Double Click Zoom', 'hip'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('On', 'hip'),
                'label_off' => __('Off', 'hip'),
                'default' => 'no',
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'hip_map_wheel_zoom_option',
            [
                'label' => __('Scroll Wheel Zoom', 'hip'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('On', 'hip'),
                'label_off' => __('Off', 'hip'),
                'default' => 'no',
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'hip_map_dragging_option',
            [
                'label' => __('Map Dragging', 'hip'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('On', 'hip'),
                'label_off' => __('Off', 'hip'),
                'default' => 'no',
                'separator' => 'before',
            ]
        );



        $this->add_control(
            'hip_maps_zoom_desktop',
            array(
                'label'   => __('Zoom Desktop', 'hip'),
                'type'    => Controls_Manager::SLIDER,

                'range'   => array(
                    'px' => array(
                        'min' => 0,
                        'max' => 22,
                    ),
                ),
                'default' => [
                    'unit' => '%',
                    'size' => 8,
                ],
                'separator' => 'before',

            )
        );
        $this->add_control(
            'hip_maps_zoom_tab',
            array(
                'label'   => __('Zoom Tab', 'hip'),
                'type'    => Controls_Manager::SLIDER,
                'default' => array(
                    'size' => 12,
                ),

                'range'   => array(
                    'px' => array(
                        'min' => 0,
                        'max' => 22,
                    ),
                ),
                'default' => [
                    'unit' => '%',
                    'size' => 10,
                ],

            )
        );

        $this->add_control(
            'hip_maps_zoom_mobile',
            array(
                'label'   => __('Zoom Mobile', 'hip'),
                'type'    => Controls_Manager::SLIDER,
                'default' => array(
                    'size' => 12,
                ),

                'range'   => array(
                    'px' => array(
                        'min' => 0,
                        'max' => 22,
                    ),
                ),
                'default' => [
                    'unit' => '%',
                    'size' => 8,
                ],

            )
        );

        $this->add_responsive_control(
            'hip_maps_map_height',
            array(
                'label'     => __('Height', 'hip'),
                'type'      => Controls_Manager::SLIDER,
                'default'   => array(
                    'size' => 500,
                ),
                'range'     => array(
                    'px' => array(
                        'min' => 80,
                        'max' => 1400,
                    ),
                ),
                'selectors' => array(
                    '{{WRAPPER}} .hip-custom-map.leaflet-container' => 'height: {{SIZE}}px;',
                ),
            )
        );
        $this->add_control(
            'hip_map_popup_show_always',
            [
                'label' => __('Info Container Always Opened', 'hip'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('Show', 'hip'),
                'label_off' => __('Hide', 'hip'),
                'default' => 'no',
                'separator' => 'before',
            ]
        );
        //Map style
        $this->add_control(
            'map_style',
            [
                'label' => __('Map Style', 'hip'),
                'type' => Controls_Manager::SELECT,
                'default' => 'CartoDB.Positron',
                'options' => [
                    'OpenStreetMap.Mapnik' => esc_html__('Open Street Map', 'hip'),
                    'Stamen.TonerLite' => esc_html__('Toner Lite', 'hip'),
                    'Esri.WorldStreetMap' => esc_html__('World Street Map', 'hip'),
                    'CartoDB.Positron' => esc_html__('Positron ', 'hip'),
                    'Stadia.AlidadeSmooth' => esc_html__('Stadia Alidade Smooth ', 'hip'),
                    'Esri.WorldGrayCanvas' => esc_html__('World Gray Canvas ', 'hip'),
                    'CartoDB.Voyager' => esc_html__('CartoDB Voyager ', 'hip'),
                ],
            ]
        );

        $this->end_controls_section();
    }
    //Style control section

    private function style_content_register_controls() {

        $this->start_controls_section(
            'hip_info_popup_style',
            [
                'label' => esc_html__( 'Content Info', 'hip' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );


        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'selector' => '{{WRAPPER}} .leaflet-popup-content a,{{WRAPPER}} .leaflet-popup-content a',
            ]
        );

        $this->add_responsive_control(
            'hip_map_info_box_radius',
            array(
                'label'      => __('Border Radius', 'hip'),
                'type'       => Controls_Manager::SLIDER,
                'size_units' => array('px', '%', 'em'),
                'default' => [
                    'unit' => 'px',
                    'size' => 5,
                ],
                'selectors'  => array(
                    '{{WRAPPER}} .leaflet-popup-content-wrapper ' => 'border-radius: {{SIZE}}{{UNIT}};overflow:hidden',
                ),
            )
        );
        $this->add_responsive_control(
            'hip_info_box_margin',
            array(
                'label'      => __('Margin', 'hip'),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array('px', 'em', '%'),
					'default' => [
							'top'=>0, 'right'=>0, 'bottom'=>33, 'left'=>9, 'unit'=>'px'
					],
                'selectors'  => array(
                    '{{WRAPPER}} .leaflet-popup.leaflet-zoom-animated' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ),
            )
        );

        $this->add_responsive_control(
            'hip_info_box_padding',
            array(
                'label'      => __('Padding', 'hip'),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array('px', 'em', '%'),
                'default' => [
                    'top'=>14, 'right'=>28, 'bottom'=>14, 'left'=>28, 'unit'=>'px'
                ],
                'selectors'  => array(
                    '{{WRAPPER}} .leaflet-popup-content-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ),
            )
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            array(
                'label'    => __('Box Shadow', 'hip'),
                'name'     => 'hip_map_shadow',
                'selector' => '{{WRAPPER}} .leaflet-popup-content-wrapper',
            )
        );

        $this->end_controls_section();

    }
    private function style_close_icon_register_controls() {

        $this->start_controls_section(
            'hip_close_icon',
            [
                'label' => esc_html__( 'Close Icon', 'hip' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,

            ]
        );

        $this->add_control(
            'hip_close_icon_toggle',
            [
                'label' => esc_html__( 'Toggle Close Button', 'hip' ),
                'type' =>Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'hip' ),
				'label_off' => esc_html__( 'Hide', 'hip' ),
				'return_value' => true,
				'default' => true,
            ]
        );

        $this->add_control(
            'hip_close_icon_color',
            [
                'label' => esc_html__( 'Icon Color', 'hip' ),
                'type' =>Controls_Manager::COLOR,
                'default'	=> '#000',
                'selectors' => [
                    '{{WRAPPER}} .leaflet-container a.leaflet-popup-close-button' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_responsive_control(
            'hip_close_icon_width',
            [
                'label' => esc_html__( 'Icon Box Width', 'hip' ),
                'type' =>Controls_Manager::NUMBER,
				'default'	=> 20,
                'size_units' => array('px', 'em', '%'),
                'selectors' => [
                    '{{WRAPPER}} .leaflet-container a.leaflet-popup-close-button' => 'width: {{VALUE}}px;',
                ],
            ]
        );

        $this->add_responsive_control(
            'hip_close_icon_height',
            [
                'label' => esc_html__( 'Icon Box Height', 'hip' ),
                'type' =>Controls_Manager::NUMBER,
				'default'	=> 20,
                'size_units' => array('px', 'em', '%'),
                'selectors' => [
                    '{{WRAPPER}} .leaflet-container a.leaflet-popup-close-button' => 'height: {{VALUE}}px; line-height: {{VALUE}}px',
                ],
            ]
        );

        $this->add_control(
            'hip_close_icon_hover_color',
            [
                'label' => esc_html__( 'Icon Hover Color', 'hip' ),
                'type' =>Controls_Manager::COLOR,
				'default'	=> '#fff',
                'selectors' => [
                    '{{WRAPPER}} #map .leaflet-popup-content-wrapper:hover ~ a.leaflet-popup-close-button' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'hip_close_bg_color',
            [
                'label' => esc_html__( 'Icon Box Color', 'hip' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#fff',
                'selectors' => [
                    '{{WRAPPER}} .leaflet-container a.leaflet-popup-close-button' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'hip_close_bg_hover_color',
            [
                'label' => esc_html__( 'Icon Box Hover Color', 'hip' ),
                'type' => Controls_Manager::COLOR,
				'default' => '#fff',
                'selectors' => [
                    '{{WRAPPER}} #map .leaflet-popup-content-wrapper:hover ~ a.leaflet-popup-close-button' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_responsive_control(
            'hip_hip_close_margin',
            array(
                'label'      => __('Margin', 'hip'),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array('px', 'em', '%'),
                'selectors'  => array(
                    '{{WRAPPER}} .leaflet-container a.leaflet-popup-close-button' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ),
            )
        );

        $this->end_controls_section();

    }
    private function style_map_box_register_controls() {

        $this->start_controls_section(
            'map-style-control',
            [
                'label' => esc_html__( 'Map ', 'hip' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'border',
                'label' => esc_html__( 'Border', 'hip' ),
                'selector' => '{{WRAPPER}} .hip-map-container ',
            ]
        );

        $this->add_responsive_control(
            'hip_map_radius',
            array(
                'label'      => __('Border Radius', 'hip'),
                'type'       => Controls_Manager::SLIDER,
                'size_units' => array('px', '%', 'em'),
                'selectors'  => array(
                    '{{WRAPPER}} .hip-map-container ' => 'border-radius: {{SIZE}}{{UNIT}};overflow:hidden',
                ),
            )
        );

        $this->add_responsive_control(
            'hip_map_box_margin',
            array(
                'label'      => __('Margin', 'hip'),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array('px', 'em', '%'),
                'selectors'  => array(
                    '{{WRAPPER}} .hip-map-container ' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ),
            )
        );

        $this->add_responsive_control(
            'hip_map_box_padding',
            array(
                'label'      => __('Padding', 'hip'),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => array('px', 'em', '%'),
                'selectors'  => array(
                    '{{WRAPPER}} .hip-map-container ' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
                ),
            )
        );

        $this->add_responsive_control(
            'hip_pin_icon_height',
            [
                'label'     => esc_html__( 'Pin Height', 'hip' ),
                'type'      => Controls_Manager::SLIDER,
                'desktop_default' => [
					'size' => 45,
					'unit' => 'px',
				],
				'tablet_default' => [
					'size' => 45,
					'unit' => 'px',
				],
				'mobile_default' => [
					'size' => 30,
					'unit' => 'px',
				],
                'selectors' => [
					'{{WRAPPER}} .leaflet-pane.leaflet-marker-pane img' => 'height: {{SIZE}}{{UNIT}} !important;',
				],
            ]
        );


        $this->add_responsive_control(
            'hip_pin_icon_width',
            [
                'label'     => esc_html__( 'Pin Width', 'hip' ),
                'type'      => Controls_Manager::SLIDER,
                'desktop_default' => [
					'size' => 45,
					'unit' => 'px',
				],
				'tablet_default' => [
					'size' => 45,
					'unit' => 'px',
				],
				'mobile_default' => [
					'size' => 30,
					'unit' => 'px',
				],
                'selectors' => [
					'{{WRAPPER}} .leaflet-pane.leaflet-marker-pane img' => 'width: {{SIZE}}{{UNIT}} !important;',
				],
            ]
        );

        $this->end_controls_section();

    }




    /**
     * Render widget output on the frontend
     */
    protected function render() {


        $settings = $this->get_settings_for_display();
        $map_pins = $settings['hip_maps_map_pins'];
        $toggle_close_btn = $settings['hip_close_icon_toggle'];
		$location_map_pins = $settings['cpt_maps_location_map_pins'];
		$hip_pin_icon_height = $settings['hip_pin_icon_height'];

        if (!empty($settings['cpt_maps_location_map_pins'])) {
            $map_pins = array_merge($map_pins, $location_map_pins);
        }


        $map_auto_fit_bounds = !empty($settings['hip_map_fit_bounds'] ) ? $settings['hip_map_fit_bounds'] : 'no';
        $centerlat = !empty($settings['hip_maps_center_lat']) ? $settings['hip_maps_center_lat'] : 30.4160534;

        $centerlong = !empty($settings['hip_maps_center_long']) ? $settings['hip_maps_center_long'] : -87.2226216;
        $map_style = !empty($settings['map_style']) ? $settings['map_style'] : 'OpenStreetMap.Mapnik';
        $map_popup_show = $settings['hip_map_popup_show_always'] ;

        //settings data
        $map_settings = array(
            'zoom_desktop'              => $settings['hip_maps_zoom_desktop']['size'],
            'zoom_tab'              => $settings['hip_maps_zoom_tab']['size'],
            'zoom_mobile'              => $settings['hip_maps_zoom_mobile']['size'],
            'map_zoom_control'	=>$settings['hip_map_zoom_control'],
            'double_click_option'	=>$settings['hip_map_dbl_click_option'],
            'mouse_wheel_option'	=>$settings['hip_map_wheel_zoom_option'],
            'map_dragging_option'	=>$settings['hip_map_dragging_option'],
            'mapstyle'           => $map_style,
            'centerlat'         => $centerlat,
            'centerlong'        => $centerlong,
            'automaticOpen'     => $map_popup_show,
            'fitBounds'         => $map_auto_fit_bounds,
        );
        $this->add_render_attribute(
            'style_wrapper',
            array(
                'class'         => 'hip-custom-map',
                'data-settings' => wp_json_encode($map_settings),

            )
        );
        ?>

        <div class="hip-map-wrapper ">
            <div class="hip-map-container ">
                <div id='map' <?php echo wp_kses_post($this->get_render_attribute_string('style_wrapper')); ?>></div>
            </div>
            <?php
            foreach ( $map_pins as $index => $pin) {


                $pin_icon = $pin['hip_pin_icon']['url'];
                $latitute = $pin['hip_map_latitude'];
                $longitude = $pin['hip_map_longitude'];
                $pin_popup_url = $pin['hip_pin_desc'];
                $pin_popup_desc = $pin['hip_pin_title'];
                $key = 'hip_map_marker_' . $index;
                $elemt_id = $pin['_id'];

                $this->add_render_attribute(
                    $key,
                    array(
                        'class'          => 'hip-pin-icon',
                        'data-lng'       => $longitude,
                        'data-lat'       => $latitute,
                        'data-icon'      =>$pin_icon,
                        'data-url'      =>$pin_popup_url,
                        'data-title'      =>$pin_popup_desc,
                        'item_id'      =>$elemt_id,


                    )
                );?>

                <div <?php echo wp_kses_post($this->get_render_attribute_string($key)); ?> >
                    <?php if (!empty($pin_popup_desc) ) : ?>
                        <div class='hip-map-info <?php echo $elemt_id; ?>'>
                            <div class='map-info-desc'><?php echo wp_kses_post($pin_popup_desc); ?></div>
                        </div>
                    <?php endif; ?>
                </div>


            <?php }

            ?>


        </div>


        <?php
    }

    protected function _content_template() {
        ?>

        <div class="hip-map-wrapper ">
            <div class="hip-map-container ">
                <div id='map' class="hip-custom-map"></div>
            </div>
        </div>
        <?php
    }


}
Plugin::instance()->widgets_manager->register_widget_type(new Hip_Maps_Elementor_Widget());

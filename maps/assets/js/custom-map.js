// "use strict";
//
// jQuery(function($) {
//
//     $(document).ready(function() {
//         var $mapElement = $(".hip-custom-map");
//         if ($mapElement.length > 0) {
//             global_initialize();
//         }
//
//         function global_initialize() {
//             var mapSettings = $mapElement.data("settings");
//             renderNewMap(mapSettings);
//         }
//
//         function renderNewMap(settings) {
//             var windowCurrentWidth = window.innerWidth;
//             var zoom = settings["zoom_desktop"];
//             if (windowCurrentWidth <= 1024) {
//                 zoom = settings["zoom_tab"];
//             }
//             if (windowCurrentWidth <= 767) {
//                 zoom = settings["zoom_mobile"];
//             }
//
//             var mapstyle = settings.mapstyle;
//             var centerLat = settings.centerlat;
//             var centerLong = settings.centerlong;
//             var autoOpen = settings.automaticOpen;
//             var fitBounds = settings.fitBounds;
//             var map_zoom_control = settings.map_zoom_control == 'yes' ? true : false;
//             var zoom_in_double_click = settings.double_click_option == 'yes' ? true : false;
//             var zoom_in_while = settings.mouse_wheel_option == 'yes' ? true : false;
//             var map_dragging = settings.map_dragging_option == 'yes' ? true : false;
//
//             var args = {
//                 zoom: zoom,
//                 mapstyle: mapstyle,
//                 zoomControl: map_zoom_control,
//                 doubleClickZoom: zoom_in_double_click,
//                 scrollWheelZoom: zoom_in_while,
//                 dragging: map_dragging,
//                 closePopupOnClick: false,
//                 center: {
//                     lat: centerLat,
//                     lng: centerLong
//                 }
//             };
//
//             var markers = $(".hip-pin-icon");
//
//             var map = L.map('map', args);
//             L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//                 attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
//             }).addTo(map);
//
//             var latLngArr = [];
//
//             markers.each(function(index) {
//                 add_marker($(this), map, autoOpen, latLngArr);
//             });
//
//             if ('yes' == fitBounds) {
//                 map.fitBounds(latLngArr, {
//                     padding: [50, 50]
//                 });
//             }
//
//             L.tileLayer.provider(`${args.mapstyle}`).addTo(map);
//             document.getElementsByClassName('leaflet-control-attribution')[0].style.display = 'none';
//         }
//
//         function add_marker(pin, map, autoOpen, latLngArr) {
//             var lat = pin.data("lat");
//             var long = pin.data("lng");
//             var pin_icon = pin.data("icon");
//             var pin_url = pin.data("url");
//             var pin_title = pin.data("title");
//             var el_id = pin.attr("item_id");
//
//             var LeafIcon = L.Icon.extend({
//                 options: {
//                     iconSize: [25, 41],
//                     iconAnchor: [22, 41],
//                     popupAnchor: [-10, -44]
//                 }
//             });
//
//             var hip_cs_icon = new LeafIcon({
//                 iconUrl: pin_icon
//             });
//
//             var pin_info_box = `<a href="${pin_url}">${pin_title}</a>`;
//
//             if ('yes' == autoOpen) {
//                 new L.marker([lat, long], {
//                     icon: hip_cs_icon
//                 }).bindPopup(`${pin_info_box}`, {
//                     autoClose: false,
//                     className: `elementor-repeater-item-${el_id}`
//                 }).addTo(map).openPopup()._popup._closeButton.addEventListener('click', (event) => event.preventDefault());
//             } else {
//                 new L.marker([lat, long], {
//                     icon: hip_cs_icon
//                 }).bindPopup(`${pin_info_box}`, {
//                     className: `elementor-repeater-item-${el_id}`
//                 }).addTo(map).on('click', function() {
//                     var closeButtons = document.querySelectorAll('.leaflet-popup-close-button');
//                     closeButtons.forEach(function(btn) {
//                         btn.addEventListener('click', function(e) {
//                             e.preventDefault();
//                         });
//                     });
//                 });
//             }
//
//             latLngArr.push([lat, long]);
//         }
//     });
// });

"use strict";

document.addEventListener("DOMContentLoaded", function() {

    var mapElement = document.querySelector(".hip-custom-map");

    if (mapElement) {
        global_initialize();
    }

    function global_initialize() {
        var mapSettings = mapElement.dataset.settings;
        renderNewMap(JSON.parse(mapSettings));
    }

    function renderNewMap(settings) {
        var windowCurrentWidth = window.innerWidth;
        var zoom = settings["zoom_desktop"];
        if (windowCurrentWidth <= 1024) {
            zoom = settings["zoom_tab"];
        }
        if (windowCurrentWidth <= 767) {
            zoom = settings["zoom_mobile"];
        }

        var mapstyle = settings.mapstyle;
        var centerLat = settings.centerlat;
        var centerLong = settings.centerlong;
        var autoOpen = settings.automaticOpen;
        var fitBounds = settings.fitBounds;
        var map_zoom_control = settings.map_zoom_control == 'yes' ? true : false;
        var zoom_in_double_click = settings.double_click_option == 'yes' ? true : false;
        var zoom_in_while = settings.mouse_wheel_option == 'yes' ? true : false;
        var map_dragging = settings.map_dragging_option == 'yes' ? true : false;

        var args = {
            zoom: zoom,
            mapstyle: mapstyle,
            zoomControl: map_zoom_control,
            doubleClickZoom: zoom_in_double_click,
            scrollWheelZoom: zoom_in_while,
            dragging: map_dragging,
            closePopupOnClick: false,
            center: {
                lat: centerLat,
                lng: centerLong
            }
        };

        var markers = document.querySelectorAll(".hip-pin-icon");
        var latLngArr = [];

        var map = L.map('map', args);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        markers.forEach(function(marker) {
            add_marker(marker, map, autoOpen, latLngArr);
        });

        if ('yes' == fitBounds) {
            map.fitBounds(latLngArr, {
                padding: [50, 50]
            });
        }

        L.tileLayer.provider(`${args.mapstyle}`).addTo(map);
        document.getElementsByClassName('leaflet-control-attribution')[0].style.display = 'none';
    }

    function add_marker(pin, map, autoOpen, latLngArr) {
        var lat = pin.dataset.lat;
        var long = pin.dataset.lng;
        var pin_icon = pin.dataset.icon;
        var pin_url = pin.dataset.url;
        var pin_title = pin.dataset.title;
        var el_id = pin.getAttribute("item_id");

        var LeafIcon = L.Icon.extend({
            options: {
                iconSize: [25, 41],
                iconAnchor: [22, 41],
                popupAnchor: [-10, -44]
            }
        });

        var hip_cs_icon = new LeafIcon({
            iconUrl: pin_icon
        });

        var pin_info_box = `<a href="${pin_url}">${pin_title}</a>`;

        if ('yes' == autoOpen) {
            new L.marker([lat, long], {
                icon: hip_cs_icon
            }).bindPopup(`${pin_info_box}`, {
                autoClose: false,
                className: `elementor-repeater-item-${el_id}`
            }).addTo(map).openPopup()._popup._closeButton.addEventListener('click', (event) => event.preventDefault());
        } else {
            new L.marker([lat, long], {
                icon: hip_cs_icon
            }).bindPopup(`${pin_info_box}`, {
                className: `elementor-repeater-item-${el_id}`
            }).addTo(map).on('click', function() {
                var closeButtons = document.querySelectorAll('.leaflet-popup-close-button');
                closeButtons.forEach(function(btn) {
                    btn.addEventListener('click', function(e) {
                        e.preventDefault();
                    });
                });
            });
        }

        latLngArr.push([lat, long]);
    }
});

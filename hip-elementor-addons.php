<?php

/**
 * Plugin Name: Hip Elementor Addons
 * Description: Elementors Addons by Hip Creative, Inc.
 * Plugin URI:  https://hip.agency/
 * Version:     1.8.3.5
 * Author:      Hip Creative
 * Author URI:  https://hip.agency/
 */

use Elementor\Hip_Team_Elementor_Widget;
use Elementor\Plugin;
use HipAddons\Includes\Addons_Integration;

if (!defined('ABSPATH')) {
	exit;
}

// Define Constants.
define('HIP_ADDONS_VERSION', '1.8.3.5');
define('HIP_ADDONS_URL', plugins_url('/', __FILE__));
define('HIP_ADDONS_PATH', plugin_dir_path(__FILE__));
define('HIP_ADDONS_FILE', __FILE__);
define('HIP_ADDONS_BASENAME', plugin_basename(HIP_ADDONS_FILE));

/*
  * Load plugin core files
  */
require_once HIP_ADDONS_PATH . 'includes/class-pa-core.php';
require_once HIP_ADDONS_PATH . 'admin/includes/admin-helper.php';
require_once HIP_ADDONS_PATH . 'includes/addons-integration.php';
if (class_exists('\seraph_accel\API')) {
    require_once HIP_ADDONS_PATH . 'cache-api.php';
}

require_once HIP_ADDONS_PATH . 'hip-activity-logs.php';

$addons = new Addons_Integration();

final class Hip_Elementor_Addons
{

	private static $_instance = null;

	public static function instance()
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public function __construct()
	{
		add_action('plugins_loaded', [$this, 'init']);
        add_action( 'wp_enqueue_scripts', [$this, 'assets'] );


    }
    public function assets(){

        $plugin_dir = plugin_dir_path(__FILE__);

        wp_enqueue_script(
          'hip-mobile-nav-js',
          plugins_url('/navigation/assets/js/hip-nav-min.js', __FILE__),
          array(),  // Add dependencies if any
          filemtime($plugin_dir . 'navigation/assets/js/hip-nav-min.js'),
          false
        );

        wp_enqueue_script(
          'hip-map-leaflet-js',
          plugins_url('/maps/assets/js/leaflet.js', __FILE__),
          array(),  // Add dependencies if any
          filemtime($plugin_dir . 'maps/assets/js/leaflet.js'),
          false
        );

        wp_enqueue_script(
          'hip-map-leaflet-provider',
          plugins_url('/maps/assets/js/leaflet-provider-min.js', __FILE__),
          array(),  // Add dependencies if any
          filemtime($plugin_dir . 'maps/assets/js/leaflet-provider-min.js'),
          false
        );

        wp_enqueue_script(
          'hip-map-custom-js',
          plugins_url('/maps/assets/js/custom-map-min.js', __FILE__),
          array(),  // Add dependencies if any
          filemtime($plugin_dir . 'maps/assets/js/custom-map-min.js'),
          false
        );
		


    }

	public function init()
	{
		add_action('elementor/controls/controls_registered', [$this, 'init_controls']);
		add_action('elementor/widgets/widgets_registered', [$this, 'init_team_widget']);
		add_action('elementor/elements/categories_registered', [$this, 'init_category'] );


	}
	/**
	 * Init widget
	 */
	public function init_team_widget()
	{

		if (function_exists('get_field')) :
			require_once(__DIR__ . '/team/hip-team.php');
			Plugin::instance()->widgets_manager->register_widget_type(new Hip_Team_Elementor_Widget());
		endif;

        require_once (__DIR__ .'/maps/maps.php');
        require_once (__DIR__ .'/navigation/hip-nav.php');
	}
	/**
	 * Init category section
	 */
	public function init_category()
	{
		Elementor\Plugin::instance()->elements_manager->add_category(
			'hip',
			[
				'title' => __('Hip Team', 'hip')
			],
			1
		);
	}
	public function init_controls()
	{
		require_once(__DIR__ . '/controls/hip-mobile-nav-menu-options.php');
		new Hip_Mobile_Nav_Menu_Options();
	}
}


//Copyright Year shortcode
add_shortcode('hip_year', function () {
	return date('Y');
});

add_action('wp_footer', array ($addons, 'googleMapsJS'));


Hip_Elementor_Addons::instance();

// Initialize the plugin
function hip_al_init() {
    if (class_exists('ActivityLog')) {
        $activity_log = new ActivityLog();
        $activity_log->activity_init();
    }
}

// Activation hook to create the log table
function hip_al_activate_plugin() {
    if (class_exists('ActivityLog')) {
        $activity_log = new ActivityLog();
        $activity_log->create_table();
        $activity_log->update_table();
    }
    // Schedule the event if it's not already scheduled
    if (!wp_next_scheduled('hip_clear_old_activity_logs')) {
        // Set the first occurrence to the next Thursday
        $next_thursday = strtotime('next Thursday');
        wp_schedule_event($next_thursday, 'every_thursday', 'hip_clear_old_activity_logs');
    }
}

//create table while active the plugin
register_activation_hook(__FILE__, 'hip_al_activate_plugin');
// Clear the scheduled event on plugin deactivation
register_deactivation_hook(__FILE__, 'hip_al_clear_thursday_clear');
add_action('plugins_loaded', 'hip_al_init');


// Define a custom schedule for every Thursday

add_filter('cron_schedules', 'hip_al_add_custom_cron_schedule');

function hip_al_add_custom_cron_schedule($schedules) {
    $schedules['every_thursday'] = array(
        'interval' => 7 * DAY_IN_SECONDS, // 7 days
        'display'  => __('Once Every Thursday', 'hip'),
    );
    return $schedules;
}

// Clear old activity logs
add_action('hip_clear_old_activity_logs', 'hip_al_clear_old_activity_logs');

function hip_al_clear_old_activity_logs() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'hip_activity_logs'; // Update with your log table name
    $week_ago = current_time('mysql', 1); // Get the current time in MySQL format
    $week_ago = date('Y-m-d H:i:s', strtotime('-1 week', strtotime($week_ago))); // Date a week ago

    // Delete logs older than one week
    $wpdb->query($wpdb->prepare("DELETE FROM $table_name WHERE log_date < %s", $week_ago));
}



function hip_al_clear_thursday_clear() {
    $timestamp = wp_next_scheduled('hip_clear_old_activity_logs');
    if ($timestamp) {
        wp_unschedule_event($timestamp, 'hip_clear_old_activity_logs');
    }
}

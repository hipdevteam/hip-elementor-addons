<?php

class ActivityLog
{

    // Define an array of allowed change types using the get_template_type_flag method
    // Define constants for static data
    const ELEMENTOR_DEFAULT_CHANGE_TYPES = [
        'header',
        'footer',
        'single-page',
        'single-post',
        'archive',
        'Other'
    ];
    /**
     * @var string $table_name
     */
    protected $table_name;

    // Method to get the table name dynamically
    protected function get_table_name() {
        global $wpdb;
        return $wpdb->prefix . 'hip_activity_logs';
    }
        //Update the table column 'user_ip"
        public function update_table()
        {
            global $wpdb;
        
            // Get the table name
            $table_name = $this->get_table_name();
        
            // Check if the 'user_ip' column exists
            $column_exists = $wpdb->get_results(
                $wpdb->prepare(
                    "SHOW COLUMNS FROM `$table_name` LIKE %s",
                    'user_ip'
                )
            );
        
            // If the column doesn't exist, add it
            if (empty($column_exists)) {
                $wpdb->query(
                    "ALTER TABLE `$table_name` 
                    ADD `user_ip` varchar(45) NOT NULL AFTER `change_details`"
                );
            }
        
        
        }
    /**
     * Initialize hooks and actions.
     */
    public function activity_init()
    {
        add_action('admin_menu', [$this, 'add_activity_logs_menu']);
        add_action('admin_init', [$this, 'register_settings']);
        add_action('rest_api_init', array($this, 'register_rest_routes'));
        add_action('post_updated', array($this, 'log_structural_changes'), 10, 3);
        // Show message after saving settings
        add_action('admin_notices', [$this, 'settings_saved_notice']);
    }

    /**
     * Add menu pages for Activity Logs and settings.
     */

    public function add_activity_logs_menu()
    {
        add_menu_page(
            __('Activity Logs', 'hip'), // Page title
            __('Activity Logs', 'hip'), // Menu title
            'manage_options', // Capability
            'hip-activity-logs', // Menu slug
            [$this, 'hip_activity_logs_page'], // Function to display content
            'dashicons-archive', // Menu icon
            6 // Position
        );

        // Add a submenu for settings
        add_submenu_page(
            'hip-activity-logs', // Parent slug
            __('Activity Log Settings', 'hip'), // Page title
            __('Settings', 'hip'), // Menu title
            'manage_options', // Capability
            'hip-activity-log-settings', // Menu slug
            [$this, 'activity_log_settings_page'] // Function to display settings page
        );
    }

    /**
     * Register plugin settings.
     */
    public function register_settings()
    {
        register_setting('activity_log_settings_group', 'activity_log_api_key', array(
            'sanitize_callback' => 'sanitize_text_field',
            'default' => '',
        ));

        register_setting('activity_log_settings_group', 'activity_log_enable_logging', array(
            'sanitize_callback' => 'intval',
            'default' => 0,
        ));

        // Add settings section
        add_settings_section(
            'activity_log_settings_section',
            __('API Settings', 'hip'),
            null,
            'hip-activity-log-settings'
        );

        // Add settings fields
        add_settings_field(
            'activity_log_api_key',
            __('AF API Key', 'hip'),
            [$this, 'activity_log_api_key_field'],
            'hip-activity-log-settings',
            'activity_log_settings_section'
        );

        add_settings_field(
            'activity_log_enable_logging',
            __('Enable Logging', 'hip'),
            [$this, 'activity_log_enable_logging_field'],
            'hip-activity-log-settings',
            'activity_log_settings_section'
        );


    }

    /**
     * Display a success message when settings are saved.
     */
    public function settings_saved_notice()
    {
        if (isset($_GET['settings-updated'])) {
            add_settings_error('activity_log_messages', 'activity_log_message', __('Settings saved successfully.', 'hip'), 'updated');
        }
        settings_errors('activity_log_messages');
    }

    /**
     * Render the API key input field.
     */
    public function activity_log_api_key_field()
    {
        $api_key = get_option('activity_log_api_key', '');
        echo '<input type="text" name="activity_log_api_key" value="' . esc_attr($api_key) . '" class="regular-text" />';
    }

    /**
     * Render the Enable Logging checkbox field.
     */
    public function activity_log_enable_logging_field()
    {
        $enable_logging = get_option('activity_log_enable_logging', 0); // Default to 0
        $checked = checked(1, $enable_logging, false);
        // Use 'for' attribute to associate the label with the checkbox
        $checkbox_id = 'activity_log_enable_logging'; // Unique ID for the checkbox
        echo '<label for="' . esc_attr($checkbox_id) . '">';
        echo '<input type="checkbox" id="' . esc_attr($checkbox_id) . '" name="activity_log_enable_logging" value="1" ' . $checked . '/>';
        echo __('Enable Logging', 'hip'); // Clickable text
        echo '</label>';


    }

    /**
     * Display the settings page.
     */
    public function activity_log_settings_page()
    {
        ?>
        <div class="wrap">
            <h1><?php _e('Activity Log Settings', 'hip'); ?></h1>
            <form method="post" action="options.php">
                <?php
                settings_fields('activity_log_settings_group');
                do_settings_sections('hip-activity-log-settings');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Create the activity log table if it doesn't exist
     */

    public function create_table()
    {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE IF NOT EXISTS {$this->get_table_name()} (
            id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
            author_name varchar(255) NOT NULL,
            post_id bigint(20) UNSIGNED NOT NULL, 
            post_type varchar(255) NOT NULL,
            post_url varchar(255) NOT NULL,
            change_type varchar(255) NOT NULL,
            before_change longtext DEFAULT NULL,
            after_change longtext DEFAULT NULL,
            change_details longtext DEFAULT NULL,
            user_ip varchar(45) NOT NULL, 
            is_homepage tinyint(1) DEFAULT 0,  
            date_logged datetime DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id)
        ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    /**
     * @param $author_name
     * @param $post_id
     * @param $post_url
     * @param $change_type
     * @param $before_data
     * @param $after_data
     * @param $description
     * @param $user_ip
     * @param $is_homepage
     * @return void
     * Insert log entry
     */
    public function log_change($author_name, $post_id, $post_url, $change_type, $before_data, $after_data, $description, $user_ip,$is_homepage)
    {
        global $wpdb;
        // Check if logging is enabled
        $enable_logging = get_option('activity_log_enable_logging', 0); // Default to 0
        if (!$enable_logging) {
            return; // Do not log if logging is disabled
        }
        // Prepare data by encoding both content and elementor data into a single JSON object
        $before_change = json_encode($before_data);  // Store before data as JSON
        $after_change = json_encode($after_data);    // Store after data as JSON

        // Insert the log entry into the database
        $wpdb->insert(
            $this->get_table_name(),
            array(
                'author_name' => $author_name,
                'post_id' => $post_id,
                'post_type' => get_post_type($post_id),
                'post_url' => $post_url,
                'change_type' => $change_type,
                'before_change' => $before_change,  // Store before data as JSON
                'after_change' => $after_change,    // Store after data as JSON
                'change_details' => $description,
                'user_ip' => $user_ip,
                'is_homepage' => $is_homepage ? 1 : 0,
                'date_logged' => current_time('mysql'), // Add current time
            )
        );
    }

    /**
     * Register REST API routes
     */
    public function register_rest_routes()
    {
        register_rest_route('activity_logs/v1', '/logs', array(
            'methods' => ['GET'],
            'callback' => array($this, 'get_activity_logs'),
            'permission_callback' => [$this, 'check_permissions'],
//            'permission_callback' => '__return_true'
        ));
    }

    /**
     * @param $request
     * @return true|WP_Error
     * WP rest api permission check
     */
    public function check_permissions($request)
    {
        // Check if the user is logged in
        if (!is_user_logged_in()) {
            return new WP_Error('rest_forbidden', __('You are not authorized to access this endpoint.'), ['status' => 401]);
        }
        error_log('User is logged in: ' . wp_get_current_user()->user_login);

//         Optional: Check for specific capabilities if needed
        if (!current_user_can('read')) {
            return new WP_Error('rest_forbidden', __('You do not have sufficient permissions.'), ['status' => 403]);
        }

        return true; // User is authorized
    }

    /**
     * @param WP_REST_Request $request
     * @return WP_REST_Response
     */
    public function get_activity_logs(WP_REST_Request $request)
    {
        global $wpdb;
        // Ensure allowed change types are defined and valid
        if (empty(self::ELEMENTOR_DEFAULT_CHANGE_TYPES) || !is_array(self::ELEMENTOR_DEFAULT_CHANGE_TYPES)) {
            return new WP_REST_Response(['error' => 'Allowed change types are not defined.'], 400);
        }

        // Sanitize allowed change types for the SQL query
        $allowed_change_types = array_map('sanitize_text_field', self::ELEMENTOR_DEFAULT_CHANGE_TYPES);
        $allowed_change_types_sql = "'" . implode("', '", array_unique($allowed_change_types)) . "'";

        // Fetch logs only for the homepage or if change type matches allowed types
        $query = $wpdb->prepare(
            "SELECT * FROM {$this->get_table_name()} 
        WHERE is_homepage = 1 OR change_type IN ($allowed_change_types_sql)
        ORDER BY date_logged DESC"
        );

        // Execute the query and check for errors
        $results = $wpdb->get_results($query);

        if ($results === null) {
            return new WP_REST_Response(['error' => 'Error fetching activity logs.'], 500);
        }

        // Check if any logs were found
        if (empty($results)) {
            return new WP_REST_Response(['message' => 'No activity logs found.'], 404);
        }

        $logs = array();
        foreach ($results as $row) {
            // Calculate human-readable time difference
            $time_diff = human_time_diff(strtotime($row->date_logged), current_time('timestamp'));

            $logs[] = array(
                'author_name' => $row->author_name,
                'post_type' => $row->post_type,
                'post_url' => $row->post_url,
                'change_type' => $row->change_type,
                'before_change' => maybe_unserialize($row->before_change),
                'after_change' => maybe_unserialize($row->after_change),
                'change_details' => $row->change_details,
                'user_ip' => $row->user_ip,
                'is_homepage' => $row->is_homepage,
                'time_ago' => $time_diff . ' ago',  // "X minutes ago"
                'exact_time' => $row->date_logged,    // Exact timestamp
            );
        }

        return new WP_REST_Response($logs, 200);
    }
    /**
     * Get the IP address of the user.
     *
     * This method checks multiple server variables to identify the user's real IP address.
     * It considers the following scenarios:
     *  - Shared internet (`HTTP_CLIENT_IP`)
     *  - Proxy or load balancer (`HTTP_X_FORWARDED_FOR`)
     *  - Direct connection (`REMOTE_ADDR`)
     *
     * @return string The user's IP address. If none is found, returns 'UNKNOWN'.
     */
    public static function get_user_ip() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            // IP from shared internet
            return $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            // IP passed from a proxy, take the first in the list
            return explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])[0];
        } elseif (!empty($_SERVER['REMOTE_ADDR'])) {
            // Direct IP address
            return $_SERVER['REMOTE_ADDR'];
        } else {
            // Fallback if no IP is detected
            return 'UNKNOWN';
        }
    }
    /**
     * Get the author details (user ID, display name, and email) from the post ID
     *
     * @param int $post_id The post ID.
     * @return array Associative array containing user ID, display name, and email.
     */
    public function get_current_user_details()
    {
        // Get the current user ID (the user performing the action)
        $current_user_id = get_current_user_id();

        // If the current user is logged in
        if ($current_user_id) {
            // Get user data
            $user = get_userdata($current_user_id);

            // If user exists, return their details
            if ($user) {
                return [
                    'user_id' => $user->ID,
                    'display_name' => $user->display_name,
                    'email' => $user->user_email
                ];
            }
        }

        // If the user is not logged in or does not exist, return empty details
        return [
            'user_id' => null,
            'display_name' => 'Unknown User',
            'email' => 'Unknown Email'
        ];
    }


    /**
     * @param $post_ID
     * @param $post_after
     * @param $post_before
     * Log data insert to database and send to api
     */
    public function log_structural_changes($post_ID, $post_after, $post_before)
    {

        // Get the ID of the homepage
        $front_page_id = get_option('page_on_front');

        // Get all public post types
        $allowed_post_types = get_post_types(array('public' => true), 'names');

        // Include Elementor template post type
        $allowed_post_types[] = 'elementor_library';
         $user_ip = $this::get_user_ip();
        $current_user_id = $this->get_current_user_details();
        $user_id = $current_user_id['user_id'];
        error_log("Author Current: $user_id");
//        error_log("Author ID:    $author_id");
        // Check if the post type is allowed
        if (in_array($post_after->post_type, $allowed_post_types)) {
            // Determine if this is a new post or an update
            $is_new_post = empty($post_before) || !isset($post_before->ID) || $post_before->ID === 0;

            // Set change type and description
            $change_type = $is_new_post ? 'create' : 'Modify Content';
            $change_description = $is_new_post ? 'New ' . ucfirst($post_after->post_type) . ' Created' : 'Content structure changed';

            // Strip tags for content comparison
            $before_content = wp_strip_all_tags($post_before->post_content);
            $after_content = wp_strip_all_tags($post_after->post_content);

            // Get Elementor data before and after
            $before_elementor_data = get_post_meta($post_before->ID, '_elementor_data', true);
            $after_elementor_data = get_post_meta($post_ID, '_elementor_data', true);

            // Decode Elementor data
            $before_elementor_json = json_decode($before_elementor_data, true);
            $after_elementor_json = json_decode($after_elementor_data, true);

            // Check if this is the homepage
            $is_homepage = ($post_after->ID == $front_page_id && get_option('show_on_front') === 'page');

            // Determine the template type
            $template_type = get_post_meta($post_ID, '_elementor_template_type', true); // Fetch custom meta for Elementor types

            $template_type_flag = $this->get_template_type_flag($template_type);
            // Update change type to include Elementor template type if available
            if ($template_type) {
                $change_type = $template_type;
            }
            if ($is_homepage) {
                $change_type = 'homepage';
            }
            error_log("template type: " . $template_type);
            // Check if the content or Elementor data has changed
            $elementor_changes = $this->compare_elementor_json($before_elementor_json, $after_elementor_json);
            // Create a unique identifier for the log entry
            $log_entry_id = $post_ID . '-' . md5(json_encode($after_elementor_json));
            // Check if the content or Elementor data has changed
            if ($before_content !== $after_content || !$elementor_changes) {
                // Prevent duplicate logging using transients
                if (!get_transient($log_entry_id)) {
                    // error_log("Detected changes. Proceeding to log.");

                    // Create structured JSON data for the log entry
                    $before_after_data = array(
                        'before' => array(
                            'content' => $before_content,
                            'elementor_data' => $before_elementor_json,  // Storing Elementor JSON data
                            'template_type' => $template_type_flag
                        ),
                        'after' => array(
                            'content' => $after_content,
                            'elementor_data' => $after_elementor_json,  // Storing Elementor JSON data
                            'template_type' => $template_type_flag
                        )
                    );


                    // Log the change
                    $this->log_change(
                        $user_id,
//                        get_the_author_meta('display_name', $post_after->post_author),
                        $post_ID,
                        get_permalink($post_ID),
                        $change_type,
                        $before_after_data['before'],
                        $before_after_data['after'],
                        $change_description,
                        $user_ip,
                        $is_homepage // Flag for homepage
                    );

                    if ($is_homepage || in_array($change_type, self::ELEMENTOR_DEFAULT_CHANGE_TYPES, true)) {
                        $this->send_change_log($change_type, get_permalink($post_ID), $post_ID);

                    }
                    error_log("Post change logged for post ID: $post_ID");

                    // Store the log entry identifier in a transient to prevent duplicate logs
                    set_transient($log_entry_id, true, HOUR_IN_SECONDS); // Set for 1 hour
                } else {
                    error_log("Duplicate log entry detected for post ID: $post_ID");
                }
            } elseif ($is_new_post) {
                if (!get_transient($log_entry_id)) {
                    $this->log_change(
                        $user_id,
//                        get_the_author_meta('display_name', $post_after->post_author),
                        $post_ID,
                        get_permalink($post_ID),
                        $change_type,
                        array('content' => 'New post created', 'template_type' => $template_type_flag),
                        array('content' => $after_content, 'template_type' => $template_type_flag),
                        'New post created',
                        $is_homepage // Flag for homepage
                    );

                    // Send change log to API
                    // $this->send_change_log($change_type, get_permalink($post_ID), $post_ID);
                    error_log("New post logged for post ID: $post_ID");

                    // Store the log entry identifier in a transient
                    set_transient($log_entry_id, true, HOUR_IN_SECONDS); // Set for 1 hour
                } else {
                    error_log("Duplicate log entry detected for new post ID: $post_ID. Transient exists.");
                }
            }
        }
    }

    /**
     * @param $before_data
     * @param $after_data
     * Compare Elementor Template json data
     */
    private function compare_elementor_json($before_data, $after_data)
    {
        $changes = [];

        // Check if both before_data and after_data are arrays
        if (!is_array($before_data) || !is_array($after_data)) {
            return $changes; // Return empty changes
        }

        // Function to recursively compare two arrays
        $recursive_compare = function ($before, $after, $path = '') use (&$changes, &$recursive_compare) {
            foreach ($before as $key => $value) {
                if (array_key_exists($key, $after)) {
                    if (is_array($value)) {
                        $recursive_compare($value, $after[$key], "$path$key.");
                    } elseif ($value !== $after[$key]) {
                        // Log changes
                        $changes[$path . $key] = [
                            'before' => $value,
                            'after' => $after[$key],
                        ];
                    }
                } else {
                    $changes[$path . $key] = [
                        'before' => $value,
                        'after' => null,
                    ];
                }
            }

            // Check for new keys in the after data
            foreach ($after as $key => $value) {
                if (!array_key_exists($key, $before)) {
                    $changes[$path . $key] = [
                        'before' => null,
                        'after' => $value,
                    ];
                }
            }
        };

        $recursive_compare($before_data, $after_data);
        return $changes;
    }

    /**
     * @param $template_type
     * @return string
     */
    private function get_template_type_flag($template_type)
    {
        switch ($template_type) {
            case 'header':
                return 'Header';
            case 'footer':
                return 'Footer';
            case 'single-page':
                return 'Single Page';
            case 'single-post':
                return 'Single Post';
            case 'archive':
                return 'Archive';
            default:
                return 'Other'; // Any other type
        }
    }

    /**
     * @param $change_type
     * @param $post_url
     * @param $post_ID
     * Home page and if Elementor page like: header,footer,single page and post data are updated then send to API
     */
    public function send_change_log($change_type, $post_url, $post_ID)
    {
        $apiKey = get_option('activity_log_api_key');

        // Check if API key is empty
        if (empty($apiKey)) {
            error_log('API Key is empty. Please set it in the Activity Log settings.');
            return; // Exit the method
        }

        // Get the home page ID
        $home_page_id = get_option('page_on_front');
        error_log("Home Page ID: $home_page_id");
        error_log("Post ID: $post_ID");

        // Sanitize and validate the provided post URL
        $post_url = esc_url_raw($post_url);
        $expected_home_url = get_permalink($home_page_id);

// Check if the modified post is the home page
        if ($home_page_id === $post_ID) {
            error_log("Post URL: $post_url");

            // Check if the provided URL matches the expected home page URL
            if ($post_url !== $expected_home_url) {
                error_log('Change log not sent: provided URL does not match the expected home page URL.');
                error_log("Post URL: $post_url");
                error_log("Expected Home Page URL: $expected_home_url");
                return; // Exit if the URLs do not match
            }

            // If it's the home page and the URL matches, check the change type
            if ($change_type) {
                error_log('Change log will be sent for the home page with change type: ' . $change_type);
            } else {
                error_log('Change log not sent: Invalid change type for the home page.');
            }

        } else {
            // Check if change type is valid against the Elementor default change types
            if (in_array($change_type, self::ELEMENTOR_DEFAULT_CHANGE_TYPES, true)) {
                error_log('Elementor change detected: ' . $change_type);
            } else {
                // Log if neither condition is met
                error_log('Change log not sent: Invalid change type.');
            }
        }


        // Define your API endpoint
        $api_url = 'https://app.agencyframework.io/api/web-utility/slack/changelog';

        // Prepare the payload
        $payload = array(
            'apiKey' => $apiKey, // Replace with your secure API key
            'changes' => sanitize_text_field($change_type),
            'url' => $expected_home_url,
        );

        // Log the payload for debugging
        error_log('Payload to be sent: ' . json_encode($payload));

        // Set up the request arguments
        $args = array(
            'body' => json_encode($payload),
            'headers' => array(
                'Content-Type' => 'application/json',
            ),
            'method' => 'POST',
            'data_format' => 'body',
        );

        // Make the POST request
        $response = wp_remote_post($api_url, $args);

        // Check for errors
        if (is_wp_error($response)) {
            error_log('Error sending change log: ' . $response->get_error_message());
        } else {
            $response_body = wp_remote_retrieve_body($response);
            $response_code = wp_remote_retrieve_response_code($response);

            // Log the full response for debugging
            error_log('API Response Code: ' . $response_code);
            error_log('API Response Body: ' . $response_body);

            // Check the response status
            if ($response_code === 200 || $response_code === 201) {
                // Successful log
                error_log('Change log sent successfully: ' . $response_body);
                // Optionally, decode the response
                $response_data = json_decode($response_body, true);
                // Check for specific success indicators in the response, if applicable
                if (isset($response_data['success']) && $response_data['success'] === true) {
                    error_log('Confirmed: Change log successfully recorded on the API.');
                } else {
                    error_log('Warning: API did not confirm successful logging.');
                }
            } else {
                // Log unexpected response
                error_log('Unexpected response: ' . $response_body);
            }
        }
    }

    /**
     * @return clear_logs
     * Clear logs table data
     */
    public function clear_logs()
    {
        global $wpdb;

        // Check nonce for security
        if (!isset($_POST['_wpnonce']) || !wp_verify_nonce($_POST['_wpnonce'], 'clear_logs_action')) {
            wp_die(__('Security check failed. Please try again.', 'hip'));
        }

        // Clear all logs from the table
        $table_name = $wpdb->prefix . 'hip_activity_logs';
        $wpdb->query("DELETE FROM $table_name");

        // Display a success message
        echo '<div class="updated notice is-dismissible"><p>' . __('All activity logs have been cleared.', 'hip') . '</p></div>';
    }

    /**
     * Display Activity log
     */
    public function hip_activity_logs_page()
    {
        global $wpdb;

        // Handle log clearing
        if (isset($_POST['clear_logs'])) {
            $this->clear_logs(); // Call the clear_logs method
        }

        $table_name = $wpdb->prefix . 'hip_activity_logs';
        $logs = $wpdb->get_results("SELECT * FROM $table_name ORDER BY date_logged DESC");

        echo '<div class="wrap">';
        echo '<div class="table-top-header">';
        echo '<h1>' . __('Activity Logs', 'hip') . '</h1>';

        // Conditionally show the clear logs button only if there are logs
        if ($logs) {
            // Clear logs button with nonce for security
            echo '<div style="text-align: right;">';
            echo '<form method="post" action="">';
            wp_nonce_field('clear_logs_action'); // Nonce for security
            echo '<input type="submit" name="clear_logs" class="button button-primary" value="' . esc_attr__('Clear Logs', 'hip') . '" onclick="return confirm(\'' . esc_js(__('Are you sure you want to clear all logs?', 'hip')) . '\');">';
            echo '</form>';
            echo '</div>';
        }
        echo '</div>';

        if ($logs) {

            ?>
            <style>
                .table-top-header {
                    display: flex;
                    justify-content: space-between;
                    margin: 20px 0;
                }

                .toggle-content {
                    display: none !important; /* Hide the checkbox */
                }

                .log-content-full {
                    display: none; /* Hide full content by default */
                }

                .toggle-content:checked + .see-more-label + .log-content-full {
                    display: block !important; /* Show the full content */
                }

                .toggle-content:checked + .see-more-label::after {
                    content: "<?php echo esc_js( __( ' See Less', 'hip' ) ); ?>"; /* Add "See Less" after label */
                }

                .see-more-label::after {
                    content: "<?php echo esc_js( __( ' See More', 'hip' ) ); ?>"; /* Default text */
                }
            </style>
            <?php
            echo '<table class="wp-list-table widefat fixed striped">';
            echo '<thead>';
            echo '<tr>';
            echo '<th>' . __('Author Name', 'hip') . '</th>';
            echo '<th>' . __('User IP', 'hip') . '</th>';
            echo '<th>' . __('Post Type', 'hip') . '</th>';
            echo '<th>' . __('Post URL', 'hip') . '</th>';
            echo '<th>' . __('Change Type', 'hip') . '</th>';
            echo '<th>' . __('Before Change', 'hip') . '</th>';
            echo '<th>' . __('After Change', 'hip') . '</th>';
            echo '<th>' . __('Details', 'hip') . '</th>';
            echo '<th>' . __('Time Ago', 'hip') . '</th>';
            echo '<th>' . __('Exact Time', 'hip') . '</th>';
            echo '</tr>';
            echo '</thead>';
            echo '<tbody>';

            foreach ($logs as $log) {
                $time_diff = human_time_diff(strtotime($log->date_logged), current_time('timestamp'));

                // Unserialize and sanitize data
                $before_change = maybe_unserialize($log->before_change);
                $after_change = maybe_unserialize($log->after_change);

                // Convert arrays to strings if needed
                if (is_array($before_change)) {
                    $before_change = implode(', ', $before_change);
                } elseif (!is_string($before_change)) {
                    $before_change = '';
                }

                if (is_array($after_change)) {
                    $after_change = implode(', ', $after_change);
                } elseif (!is_string($after_change)) {
                    $after_change = '';
                }
                $user_id = $log->author_name; // Assuming author_name contains user_id

                // Ensure user_id is a valid integer before proceeding
                if ($user_id && is_numeric($user_id)) {
                    // Get user data
                    $user = get_user_by('id', $user_id);

                    // Check if user data is retrieved successfully
                    if ($user) {
                        // Get user avatar (size 32px)
                        $avatar = get_avatar($user_id, 32);

                        // Get user roles, handle case where user might have no roles
                        $roles = !empty($user->roles) ? implode(', ', $user->roles) : 'No roles assigned';

                    } else {
                        // Handle case when user is not found
                        $avatar = ''; // Or a default avatar
                        $roles = 'User not found';
                    }
                } else {
                    // Handle case when user_id is invalid
                    $avatar = ''; // Or a default avatar
                    $roles = 'Invalid User ID';
                }

                // Truncate content for before and after change
                $max_length = 100;
                $short_before = (strlen($before_change) > $max_length) ? substr($before_change, 0, $max_length) . '...' : $before_change;
                $short_after = (strlen($after_change) > $max_length) ? substr($after_change, 0, $max_length) . '...' : $after_change;
                echo '<tr>';
                echo "<td><div style='display: flex; align-items: center;'>
                        <div style='margin-right: 10px;'>$avatar</div>
                        <div>
                            <strong>" . esc_html($user->display_name) . "</strong><br>
                            <span>" . esc_html($roles) . "</span><br>
                            <span>" . esc_html($user->user_email) . "</span>
                        </div>
                      </div></td>";
//                echo '<td>' . esc_html($log->author_name) . '</td>';
                echo '<td>' . esc_html($log->user_ip) . '</td>';
                echo '<td>' . esc_html($log->post_type) . '</td>';
                echo '<td><a href="' . esc_url($log->post_url) . '" target="_blank">' . esc_html($log->post_url) . '</a></td>';
                echo '<td>' . esc_html($log->change_type) . '</td>';

                // Before Change - using CSS for toggle
                echo '<td>';
                echo '<div class="log-content-short">' . esc_html($short_before) . '</div>';
                echo '<input type="checkbox" id="toggle-before-' . esc_attr($log->id) . '" class="toggle-content" />';
                echo '<label for="toggle-before-' . esc_attr($log->id) . '" class="see-more-label button"></label>';
                echo '<div class="log-content-full" style="display:none;">' . esc_html($before_change) . '</div>';
                echo '</td>';

                // After Change - using CSS for toggle
                echo '<td>';
                echo '<div class="log-content-short">' . esc_html($short_after) . '</div>';
                echo '<input type="checkbox" id="toggle-after-' . esc_attr($log->id) . '" class="toggle-content" />';
                echo '<label for="toggle-after-' . esc_attr($log->id) . '" class="see-more-label button"></label>';
                echo '<div class="log-content-full" style="display:none;">' . esc_html($after_change) . '</div>';
                echo '</td>';

                echo '<td>' . esc_html($log->change_details) . '</td>';
                echo '<td>' . esc_html($time_diff) . ' ' . __('ago', 'hip') . '</td>';
                echo '<td>' . esc_html($log->date_logged) . '</td>';
                echo '</tr>';
            }

            echo '</tbody>';
            echo '</table>';
        } else {
            echo '<p>' . __('No activity logs found.', 'hip') . '</p>';
        }

        echo '</div>';
    }

}

// "use strict";
//
// ;
// (function ($) {
//     'use strict';
//
//     function debounce(func, wait, immediate) {
//         var timeout;
//         return function() {
//             var context = this,
//                 args = arguments;
//             var later = function later() {
//                 timeout = null;
//                 if (!immediate) func.apply(context, args);
//             };
//             var callNow = immediate && !timeout;
//             clearTimeout(timeout);
//             timeout = setTimeout(later, wait);
//             if (callNow) func.apply(context, args);
//         };
//     }
//
// // Define your NavigationMenu function
//     function NavigationMenu() {
//         var navMenu = $('.hip-nav-menu');
//         var humBurgerBtn = navMenu.find('.hip-menu-toggler');
//         var indicator = navMenu.find('.hip-submenu-indicator-wrap');
//
//         // Function to handle submenu toggle
//         function handleSubMenuClick(e) {
//             e.preventDefault();
//             e.stopPropagation();
//             // var $parentEl = $(this).parent('li.menu-item-has-children');
//             var $parentEl = $(this).closest('li.menu-item-has-children');
//
//             console.log('parent',$parentEl);
//             if ($parentEl) {
//                 $parentEl.children('ul.hip-sub-menu').slideToggle();
//             }
//         }
//
//         // Function to handle hamburger menu toggle
//         function handleHamburgerMenu(e) {
//             e.preventDefault();
//             var humberger = $(this).data('humberger');
//             var $pel = navMenu.find('ul.menu');
//             if ('open' == humberger) {
//                 $('.hip-menu-open-icon').addClass('hide-icon');
//                 $('.hip-menu-close-icon').removeClass('hide-icon');
//                 $('.hip-menu-close-icon').addClass('show-icon');
//                 $pel.slideDown();
//             } else {
//                 $('.hip-menu-close-icon').addClass('hide-icon');
//                 $('.hip-menu-open-icon').removeClass('hide-icon');
//                 $('.hip-menu-open-icon').addClass('show-icon');
//                 $pel.slideUp();
//             }
//         }
//
//         // Tablet & Mobile Menu Logic
//         function updateMenu() {
//             if ($(window).width() < 1025) {
//                 // Bind click event for submenu indicators
//                 indicator.off('click').on('click', handleSubMenuClick);
//
//                 // Bind click event for hamburger button
//                 humBurgerBtn.off('click').on('click', handleHamburgerMenu);
//
//                 // Apply mobile/tablet specific classes
//                 navMenu.removeClass('hip-navigation-menu-wrapper').addClass('hip-navigation-burger-menu');
//             } else {
//                 // Remove inline styles and classes for desktop
//                 navMenu.find('ul.menu').removeAttr('style');
//                 navMenu.find('ul.hip-sub-menu').removeAttr('style');
//                 navMenu.removeClass('hip-navigation-burger-menu').addClass('hip-navigation-menu-wrapper');
//             }
//         }
//
//         // Initial call to update menu on page load
//         updateMenu();
//
//         // Call updateMenu on window resize
//         $(window).on('resize', debounce(updateMenu, 100));
//     }
//
//     NavigationMenu();
//
//     // Call your NavigationMenu function
//    // elementorFrontend.hooks.addAction("frontend/element_ready/hip-nav-id.default", NavigationMenu);
//
// })(jQuery);
//
"use strict";

document.addEventListener('DOMContentLoaded', function() {
    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this,
                args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    function NavigationMenu() {
        var navMenu = document.querySelector('.hip-nav-menu'); // Change this selector to match your theme
        var humBurgerBtn = navMenu.querySelector('.hip-menu-toggler'); // Change this selector to match your theme
        var closeIcon = navMenu.querySelector('.hip-menu-close-icon'); // Change this selector to match your theme

        function handleSubMenuClick(e) {
            var menuItem = this.closest('.menu-item');
            var subMenu = menuItem.querySelector('ul.hip-sub-menu');
            var menuHighlighted = menuItem.querySelector('.hip-nav-item-container'); // Change this selector to match your theme
            var hasLink = this.getAttribute('href') !== '#';

            if (!subMenu && hasLink) {
                // If no submenu and the link is valid, navigate to the link
                return true; // Allow the default link behavior
            }

            e.preventDefault();
            e.stopPropagation();

            if (!subMenu) {
                return;
            }

            var menuItemIsShow = subMenu.classList.contains('show');

            if (menuItem.classList.contains('menu-item-has-children')) {
                subMenu.classList.toggle('show');
                menuHighlighted.classList.toggle('highlighted');
                if (menuItemIsShow && hasLink) {
                    subMenu.classList.remove('show');
                    menuItem.classList.remove('highlighted');
                    window.location.href = this.getAttribute('href');
                }
            }
        }

        function handleHamburgerMenu(e) {
            e.preventDefault();
            var navWrapper = navMenu.querySelector('.hip-nav-wrapper'); // Change this selector to match your theme
            var humberger = this.dataset.humberger;
            var bodyClass = document.querySelector('body');
            if (humberger === 'open') {
                document.querySelector('.hip-menu-open-icon').classList.add('hide-icon'); // Change this selector to match your theme
                closeIcon.classList.remove('hide-icon');
                closeIcon.classList.add('show-icon');
                navWrapper.classList.add('active');
                navWrapper.style.visibility = 'visible';
                bodyClass.classList.add('hip-menu-open');
            } else {
                closeIcon.classList.add('hide-icon');
                document.querySelector('.hip-menu-open-icon').classList.remove('hide-icon'); // Change this selector to match your theme
                document.querySelector('.hip-menu-open-icon').classList.add('show-icon'); // Change this selector to match your theme
                navWrapper.classList.remove('active');
                navWrapper.style.visibility = 'hidden';
                bodyClass.classList.remove('hip-menu-open');
            }
        }

        function handleCloseIconClick(e) {
            e.preventDefault();
            var bodyClass = document.querySelector('body');
            var navWrapper = navMenu.querySelector('.hip-nav-wrapper'); // Change this selector to match your theme
            closeIcon.classList.add('hide-icon');
            document.querySelector('.hip-menu-open-icon').classList.remove('hide-icon'); // Change this selector to match your theme
            document.querySelector('.hip-menu-open-icon').classList.add('show-icon'); // Change this selector to match your theme
            navWrapper.classList.remove('active');
            bodyClass.classList.remove('hip-menu-open');
        }

        function updateMenu() {
            var navItemContainers = navMenu.querySelectorAll('.hip-nav-item-container'); // Change this selector to match your theme

            if (window.innerWidth < 1025) {
                var links = document.querySelectorAll('.hip-nav-wrapper ul li .hip-nav-item-container a'); // Change this selector to match your theme
                links.forEach(function(link) {
                    link.removeEventListener('click', handleSubMenuClick);
                    link.addEventListener('click', handleSubMenuClick);
                });
                humBurgerBtn.removeEventListener('click', handleHamburgerMenu);
                humBurgerBtn.addEventListener('click', handleHamburgerMenu);

                closeIcon.removeEventListener('click', handleCloseIconClick);
                closeIcon.addEventListener('click', handleCloseIconClick);

                navMenu.classList.remove('hip-navigation-menu-wrapper');
                navMenu.classList.add('hip-navigation-burger-menu');
            } else {
                var subMenus = navMenu.querySelectorAll('ul.hip-sub-menu');
                subMenus.forEach(function(subMenu) {
                    subMenu.classList.remove('show');
                });
                navItemContainers.forEach(function(container) {
                    container.removeEventListener('click', handleSubMenuClick);
                });
                humBurgerBtn.removeEventListener('click', handleHamburgerMenu);
                closeIcon.removeEventListener('click', handleCloseIconClick);
                navMenu.classList.remove('hip-navigation-burger-menu');
                navMenu.classList.add('hip-navigation-menu-wrapper');
            }
        }

        // Initial call to update menu on page load
        updateMenu();

        // Call updateMenu on window resize
        window.addEventListener('resize', debounce(updateMenu, 100));
    }

    NavigationMenu();
});

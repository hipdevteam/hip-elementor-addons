// "use strict";
//
// ;
// (function ($) {
//     'use strict';
//
//     function debounce(func, wait, immediate) {
//         var timeout;
//         return function() {
//             var context = this,
//                 args = arguments;
//             var later = function later() {
//                 timeout = null;
//                 if (!immediate) func.apply(context, args);
//             };
//             var callNow = immediate && !timeout;
//             clearTimeout(timeout);
//             timeout = setTimeout(later, wait);
//             if (callNow) func.apply(context, args);
//         };
//     }
//
// // Define your NavigationMenu function
//     function NavigationMenu() {
//         var navMenu = $('.hip-nav-menu');
//         var humBurgerBtn = navMenu.find('.hip-menu-toggler');
//         var indicator = navMenu.find('.hip-submenu-indicator-wrap');
//
//         // Function to handle submenu toggle
//         function handleSubMenuClick(e) {
//             e.preventDefault();
//             e.stopPropagation();
//             // var $parentEl = $(this).parent('li.menu-item-has-children');
//             var $parentEl = $(this).closest('li.menu-item-has-children');
//
//             console.log('parent',$parentEl);
//             if ($parentEl) {
//                 $parentEl.children('ul.hip-sub-menu').slideToggle();
//             }
//         }
//
//         // Function to handle hamburger menu toggle
//         function handleHamburgerMenu(e) {
//             e.preventDefault();
//             var humberger = $(this).data('humberger');
//             var $pel = navMenu.find('ul.menu');
//             if ('open' == humberger) {
//                 $('.hip-menu-open-icon').addClass('hide-icon');
//                 $('.hip-menu-close-icon').removeClass('hide-icon');
//                 $('.hip-menu-close-icon').addClass('show-icon');
//                 $pel.slideDown();
//             } else {
//                 $('.hip-menu-close-icon').addClass('hide-icon');
//                 $('.hip-menu-open-icon').removeClass('hide-icon');
//                 $('.hip-menu-open-icon').addClass('show-icon');
//                 $pel.slideUp();
//             }
//         }
//
//         // Tablet & Mobile Menu Logic
//         function updateMenu() {
//             if ($(window).width() < 1025) {
//                 // Bind click event for submenu indicators
//                 indicator.off('click').on('click', handleSubMenuClick);
//
//                 // Bind click event for hamburger button
//                 humBurgerBtn.off('click').on('click', handleHamburgerMenu);
//
//                 // Apply mobile/tablet specific classes
//                 navMenu.removeClass('hip-navigation-menu-wrapper').addClass('hip-navigation-burger-menu');
//             } else {
//                 // Remove inline styles and classes for desktop
//                 navMenu.find('ul.menu').removeAttr('style');
//                 navMenu.find('ul.hip-sub-menu').removeAttr('style');
//                 navMenu.removeClass('hip-navigation-burger-menu').addClass('hip-navigation-menu-wrapper');
//             }
//         }
//
//         // Initial call to update menu on page load
//         updateMenu();
//
//         // Call updateMenu on window resize
//         $(window).on('resize', debounce(updateMenu, 100));
//     }
//
//     NavigationMenu();
//
//     // Call your NavigationMenu function
//    // elementorFrontend.hooks.addAction("frontend/element_ready/hip-nav-id.default", NavigationMenu);
//
// })(jQuery);
//
"use strict";

document.addEventListener('DOMContentLoaded', function() {
    function debounce(func, wait, immediate) {
        let timeout;
        return function() {
            const context = this, args = arguments;
            const later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            const callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    function NavigationMenu() {
        const navMenu = document.querySelector('.hip-nav-menu');
        if (!navMenu) {
            console.warn('Navigation menu not found');
            return;
        }

        const humBurgerBtn = navMenu.querySelector('.hip-menu-toggler');
        if (!humBurgerBtn) {
            console.warn('Hamburger button not found');
            return;
        }

        function handleSubMenuClick(e) {
            const menuItem = this.closest('.menu-item');
            if (!menuItem) {
                console.warn('Menu item not found');
                return;
            }

            const subMenu = menuItem.querySelector('ul.hip-sub-menu');
            const menuHighlighted = menuItem.querySelector('.hip-nav-item-container');
            const hasLink = this.getAttribute('href') !== '#';

            if (!subMenu && hasLink) {
                if (this.getAttribute('href').includes('#')) {
                    closeMenu();
                    setTimeout(() => window.location.href = this.getAttribute('href'), 100);
                } else {
                    return true;
                }
            }

            e.preventDefault();
            e.stopPropagation();

            if (!subMenu) {
                return;
            }

            const menuItemIsShow = subMenu.classList.contains('show');

            if (menuItem.classList.contains('menu-item-has-children')) {
                subMenu.classList.toggle('show');
                if (menuHighlighted) {
                    menuHighlighted.classList.toggle('highlighted');
                } else {
                    console.warn('Menu highlight container not found');
                }
                if (menuItemIsShow && hasLink) {
                    subMenu.classList.remove('show');
                    if (menuHighlighted) {
                        menuHighlighted.classList.remove('highlighted');
                    }
                    setTimeout(() => window.location.href = this.getAttribute('href'), 100);
                    closeMenu();
                }
            }
        }

        function handleIndicatorClick(e) {
            e.preventDefault();
            e.stopPropagation();

            const menuItem = this.closest('.menu-item');
            if (!menuItem) {
                console.warn('Menu item not found');
                return;
            }

            const subMenu = menuItem.querySelector('ul.hip-sub-menu');
            const menuHighlighted = menuItem.querySelector('.hip-nav-item-container');

            if (subMenu) {
                subMenu.classList.toggle('show');
                if (menuHighlighted) {
                    menuHighlighted.classList.toggle('highlighted');
                } else {
                    console.warn('Menu highlight container not found');
                }
            }
        }

        function handleHamburgerMenu(e) {
            e.preventDefault();
            const navWrapper = navMenu.querySelector('.hip-nav-wrapper');
            if (!navWrapper) {
                console.warn('Nav wrapper not found');
                return;
            }

            const bodyClass = document.body;
            const subMenu = document.querySelector('ul.hip-sub-menu');
            if (!bodyClass || !subMenu) {
                console.warn('Body or submenu not found');
                return;
            }
            if(humBurgerBtn.classList.contains('open_icon')){
                closeMenu();
            } else {
                humBurgerBtn.classList.add("open_icon");
                navWrapper.classList.add('active');
                navWrapper.style.visibility = 'visible';
                bodyClass.classList.add('hip-menu-open');
            }
        }

        function handleContactListClick(e) {
            const href = this.getAttribute('href');
            if (href && href.includes('#')) {
                e.preventDefault();
                closeMenu();
                setTimeout(() => window.location.href = href, 100);
            }
        }

        function closeMenu() {
            const navWrapper = navMenu.querySelector('.hip-nav-wrapper');
            const bodyClass = document.body;
            const subMenu = document.querySelector('ul.hip-sub-menu');
            if (!navWrapper || !bodyClass || !subMenu) {
                console.warn('Nav wrapper, body, or submenu not found');
                return;
            }
            humBurgerBtn.classList.remove("open_icon");
            navWrapper.classList.remove('active');
            navWrapper.style.visibility = 'hidden';
            bodyClass.classList.remove('hip-menu-open');
            subMenu.classList.remove('show');
        }

        function updateMenu() {
            const contactListLinks = document.querySelectorAll('.hip-nav-contact-list a');
            const indicators = navMenu.querySelectorAll('.hip-submenu-indicator-wrap');
            const navItemContainers = navMenu.querySelectorAll('.hip-nav-item-container');

            if (window.innerWidth < 1025) {
                indicators.forEach(function(indicator) {
                    indicator.removeEventListener('click', handleIndicatorClick);
                    indicator.addEventListener('click', handleIndicatorClick);
                });

                const links = navMenu.querySelectorAll('.hip-nav-wrapper ul li .hip-nav-item-container a');
                links.forEach(function(link) {
                    link.removeEventListener('click', handleSubMenuClick);
                    link.addEventListener('click', handleSubMenuClick);
                });
                contactListLinks.forEach(function(link) {
                    link.removeEventListener('click', handleContactListClick);
                    link.addEventListener('click', handleContactListClick);
                });
                humBurgerBtn.removeEventListener('click', handleHamburgerMenu);
                humBurgerBtn.addEventListener('click', handleHamburgerMenu);

                navMenu.classList.remove('hip-navigation-menu-wrapper');
                navMenu.classList.add('hip-navigation-burger-menu');
            } else {
                const subMenus = navMenu.querySelectorAll('ul.hip-sub-menu');
                subMenus.forEach(function(subMenu) {
                    subMenu.classList.remove('show');
                });
                navItemContainers.forEach(function(container) {
                    container.removeEventListener('click', handleSubMenuClick);
                });
                humBurgerBtn.removeEventListener('click', handleHamburgerMenu);
                navMenu.classList.remove('hip-navigation-burger-menu');
                navMenu.classList.add('hip-navigation-menu-wrapper');
            }
        }

        // Initial call to update menu on page load
        updateMenu();

        // Call updateMenu on window resize
        window.addEventListener('resize', debounce(updateMenu, 100));
    }

    NavigationMenu();
});

<?php

use Elementor\Plugin;
use Elementor\Widget_Base;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;
use Elementor\Group_Control_Background;
use Elementor\Icons_Manager;
use Elementor\Repeater;

defined( 'ABSPATH' ) || die();

require_once (__DIR__ .'/hip-walker-nav-menu.php');

class Hip_Nav extends Widget_Base {
    /**
     * construct
     */
    public function __construct($data = [], $args = null)
    {
        parent::__construct($data, $args);
         wp_register_style('hip-nav-css', plugins_url('/assets/css/hip-nav-min.css', __FILE__), array(), time());
    }

    public function get_style_depends()
    {
        return ['hip-nav-css'];
    }
//    public function get_script_depends()
//    {
//        return array(
//          'hip-mobile-nav-js',
//        );
//    }
    public function get_name()
    {

        return 'hip-nav-id';
    }
    /**
     * Get widget title.
     */
    public function get_title() {
        return __( 'HIP Nav Menu', 'hip' );
    }

    /**
     * Get widget icon.
     */
    public function get_icon() {
        return 'eicon-nav-menu';
    }

    public function get_keywords() {
        return [ 'nav', 'menu', 'hip nav menu', 'hip navigation', 'Nav Menu', 'Navigation Menu' ];
    }

    /**
     * Get a list of all Navigation Menu
     *
     * @return array
     */
    public function get_menus() {
        $list  = [];
        $menus = wp_get_nav_menus();
        foreach ( $menus as $menu ) {
            $list[ $menu->slug ] = $menu->name;
        }

        return $list;
    }
    /**
     * Register widget content controls
     */
    protected function register_controls() {
        $this->hip_nav_menu_content_controls();
        $this->register_style_controls();
        $this->menu_list_items_controls();
        $this->menu_social_icons_controls();
        $this->menu_cta_button_controls();
    }

    protected function hip_nav_menu_content_controls() {


        $this->start_controls_section(
          '_hip_section_nav_menu_settings',
          [
            'label' => __( 'HIP Nav Menu', 'hip' ),
            'tab'   => Controls_Manager::TAB_CONTENT,
          ]
        );

        $this->add_control(
          'hip_nav_menu_list',
          [
            'label'   => __( 'Select menu', 'hip' ),
            'type'    => Controls_Manager::SELECT,
            'options' => $this->get_menus(),
          ]
        );

        $this->add_control(
          'hip_nav_menu_position',
          [
            'label'     => __( 'Alignment', 'hip' ),
            'type'      => Controls_Manager::CHOOSE,
            'options'   => [
              'flex-start' => [
                'title' => __( 'Left', 'hip' ),
                'icon'  => 'eicon-h-align-left',
              ],
              'center'     => [
                'title' => __( 'Center', 'hip' ),
                'icon'  => 'eicon-h-align-center',
              ],
              'flex-end'   => [
                'title' => __( 'Right', 'hip' ),
                'icon'  => 'eicon-h-align-right',
              ],
            ],
            'toggle'    => true,
            'default'   => 'flex-end',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu' => 'justify-content: {{VALUE}};',
            ],
          ]
        );

        $this->add_control(
          '_hip_heading_nav_menu_responsive',
          [
            'label'     => __( 'Responsive', 'hip' ),
            'type'      => Controls_Manager::HEADING,
            'separator' => 'before',
          ]
        );

        $this->add_control(
          'hip_nav_menu_responsive_position',
          [
            'label'     => __( 'Alignment', 'hip' ),
            'type'      => Controls_Manager::CHOOSE,
            'options'   => [
              'flex-start' => [
                'title' => __( 'Left', 'hip' ),
                'icon'  => 'eicon-h-align-left',
              ],
              'center'     => [
                'title' => __( 'Center', 'hip' ),
                'icon'  => 'eicon-h-align-center',
              ],
              'flex-end'   => [
                'title' => __( 'Right', 'hip' ),
                'icon'  => 'eicon-h-align-right',
              ],
            ],
            'toggle'    => true,
            'default'   => 'flex-end',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu .hip-nav-humberger-wrapper' => 'justify-content: {{VALUE}};',
            ],
          ]
        );
        $this->add_control(
          'hip_hamburger_icon_styles',
          [
            'label' => esc_html__( 'Hamburger Icon', 'hip' ),
            'type' => Controls_Manager::SELECT,
            'default' => 'v1',
            'options' => [
              'v1' => esc_html__( 'Icon 1', 'hip' ),
              'v2' => esc_html__( 'Icon 2', 'hip' ),
              'v3' => esc_html__( 'Icon 3', 'hip' ),
              'v4' => esc_html__( 'Icon 4', 'hip' ),
              'v5' => esc_html__( 'Icon 5', 'hip' ),
            ],
          ]
        );

        $this->add_control(
          'enabled_menu_contact_list',
          [
            'label' => esc_html__( 'Enable Menu Contact', 'hip' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => esc_html__( 'Show', 'hip' ),
            'label_off' => esc_html__( 'Hide', 'hip' ),
            'return_value' => 'yes',
            'default' => 'no',

          ]
        );
        $this->add_control(
          'enabled_menu_social_icons',
          [
            'label' => esc_html__( 'Enable menu Social icons', 'hip' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => esc_html__( 'Show', 'hip' ),
            'label_off' => esc_html__( 'Hide', 'hip' ),
            'return_value' => 'yes',
            'default' => 'no',

          ]
        );
        $this->add_control(
          'enabled_menu_cta_button',
          [
            'label' => esc_html__( 'Enable menu CTA Button', 'hip' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => esc_html__( 'Show', 'hip' ),
            'label_off' => esc_html__( 'Hide', 'hip' ),
            'return_value' => 'yes',
            'default' => 'no',

          ]
        );
        $this->end_controls_section();
    }
    protected function menu_list_items_controls(){
        $this->start_controls_section(
          'content_section',
          [
            'label' => esc_html__( 'Menu Contact List', 'hip' ),
            'tab' => Controls_Manager::TAB_CONTENT,
            'condition' => [
              'enabled_menu_contact_list' => 'yes',
            ],
          ]
        );
        $repeater =  new Repeater();
        $repeater->add_control(
          'hip_nav_list_text',
          [
            'label' => esc_html__( 'Text', 'hip' ),
            'type' => Controls_Manager::TEXT,
            'label_block' => true,
            'placeholder' => esc_html__( 'List Item', 'hip' ),
            'default' => esc_html__( 'List Item', 'hip' ),
            'dynamic' => [
              'active' => true,
            ],
          ]
        );

        $repeater->add_control(
          'hip_nav_selected_icon',
          [
            'label' => esc_html__( 'Icon', 'hip' ),
            'type' => Controls_Manager::ICONS,
            'default' => [
              'value' => 'fas fa-check',
              'library' => 'fa-solid',
            ],
            'fa4compatibility' => 'icon',
          ]
        );

        $repeater->add_control(
          'hip_nav_menu_link',
          [
            'label' => esc_html__( 'Link', 'hip' ),
            'type' => Controls_Manager::URL,
            'dynamic' => [
              'active' => true,
            ],
          ]
        );
// Adding ID control
        $repeater->add_control(
          'hip_nav_list_id',
          [
            'label' => esc_html__( 'CSS ID', 'hip' ),
            'type' => Controls_Manager::TEXT,
            'label_block' => true,
            'placeholder' => esc_html__( 'Unique ID', 'hip' ),
            'default' => '',
          ]
        );
        $this->add_control(
          'hip_nav_contact_icon_list',
          [
            'label' => esc_html__( 'Items', 'hip' ),
            'type' => Controls_Manager::REPEATER,
            'fields' => $repeater->get_controls(),
            'default' => [
              [
                'hip_nav_list_text' => esc_html__( 'List Item #1', 'hip' ),
                'hip_nav_selected_icon' => [
                  'value' => 'fas fa-check',
                  'library' => 'fa-solid',
                ],
              ],
              [
                'hip_nav_list_text' => esc_html__( 'List Item #2', 'hip' ),
                'hip_nav_selected_icon' => [
                  'value' => 'fas fa-times',
                  'library' => 'fa-solid',
                ],
              ],
              [
                'hip_nav_list_text' => esc_html__( 'List Item #3', 'hip' ),
                'hip_nav_selected_icon' => [
                  'value' => 'fas fa-dot-circle',
                  'library' => 'fa-solid',
                ],
              ],
            ],
            'title_field' => '{{{ elementor.helpers.renderIcon( this, hip_nav_selected_icon, {}, "i", "panel" ) || \'<i class="{{ icon }}" aria-hidden="true"></i>\' }}} {{{ hip_nav_list_text }}}',
          ]
        );
        $this->end_controls_section();
    }
    protected function menu_social_icons_controls() {
        $this->start_controls_section(
          'hip_social_icons_section',
          [
            'label' => esc_html__('Menu Social Icons', 'hip'),
            'tab' => Controls_Manager::TAB_CONTENT,
            'condition' => [
              'enabled_menu_social_icons' => 'yes',
            ],
          ]
        );

        $repeater = new Repeater();

        $repeater->add_control(
          'hip_nav_social_icon_text',
          [
            'label' => esc_html__('Text', 'hip'),
            'type' => Controls_Manager::TEXT,
            'label_block' => true,
            'placeholder' => esc_html__('List Item', 'hip'),
            'default' => esc_html__('List Item', 'hip'),
            'dynamic' => [
              'active' => true,
            ],
          ]
        );

        $repeater->add_control(
          'hip_nav_selected_social_icon',
          [
            'label' => esc_html__('Icon', 'hip'),
            'type' => Controls_Manager::ICONS,
            'default' => [
              'value' => 'fas fa-check',
              'library' => 'fa-solid',
            ],
            'fa4compatibility' => 'icon',
          ]
        );

        $repeater->add_control(
          'hip_nav_menu_social_icon_link',
          [
            'label' => esc_html__('Link', 'hip'),
            'type' => Controls_Manager::URL,
            'dynamic' => [
              'active' => true,
            ],
          ]
        );

        // Adding ID control
        $repeater->add_control(
          'hip_nav_social_icon_id',
          [
            'label' => esc_html__('CSS ID', 'hip'),
            'type' => Controls_Manager::TEXT,
            'label_block' => true,
            'placeholder' => esc_html__('Unique ID', 'hip'),
            'default' => '',
          ]
        );

        $this->add_control(
          'hip_nav_social_icon_lists',
          [
            'label' => esc_html__('Items', 'hip'),
            'type' => Controls_Manager::REPEATER,
            'fields' => $repeater->get_controls(),
            'default' => [
              [
                'hip_nav_social_icon_text' => esc_html__('Facebook', 'hip'),
                'hip_nav_selected_social_icon' => [
                  'value' => 'fab fa-facebook',
                  'library' => 'fa-brands',
                ],
                'hip_nav_menu_social_icon_link' => [
                  'url' => 'https://facebook.com',
                ],
              ],
              [
                'hip_nav_social_icon_text' => esc_html__('Instagram', 'hip'),
                'hip_nav_selected_social_icon' => [
                  'value' => 'fab fa-instagram',
                  'library' => 'fa-brands',
                ],
                'hip_nav_menu_social_icon_link' => [
                  'url' => 'https://instagram.com',
                ],
              ],
              [
                'hip_nav_social_icon_text' => esc_html__('YouTube', 'hip'),
                'hip_nav_selected_social_icon' => [
                  'value' => 'fab fa-youtube',
                  'library' => 'fa-brands',
                ],
                'hip_nav_menu_social_icon_link' => [
                  'url' => 'https://youtube.com',
                ],
              ],
            ],
            'title_field' => '{{{ elementor.helpers.renderIcon( this, hip_nav_selected_social_icon, {}, "i", "panel" ) || \'<i class="{{ icon }}" aria-hidden="true"></i>\' }}} {{{ hip_nav_social_icon_text }}}',
          ]
        );

        $this->end_controls_section();
    }
    protected function menu_cta_button_controls() {
        $this->start_controls_section(
          'hip_cta_button_section',
          [
            'label' => esc_html__('Menu CTA Button', 'hip'),
            'tab' => Controls_Manager::TAB_CONTENT,
            'condition' => [
              'enabled_menu_cta_button' => 'yes',
            ],
          ]
        );
        $this->add_control(
          'hip_menu_cta_button_text',
          [
            'label' =>  esc_html__( 'Click here', 'hip' ),
            'type' => Controls_Manager::TEXT,
            'dynamic' => [
              'active' => true,
            ],
            'default' =>  esc_html__( 'Click here', 'hip' ),
            'placeholder' =>  esc_html__( 'Click here', 'hip' ),

          ]
        );

        $this->add_control(
          'hip_menu_cta_button_link',
          [
            'label' => esc_html__( 'Link', 'hip' ),
            'type' => Controls_Manager::URL,
            'dynamic' => [
              'active' => true,
            ],
            'default' => [
              'url' => '#',
            ],

          ]
        );

        $this->add_control(
          'hip_menu_cta_button_selected_icon',
          [
            'label' => esc_html__( 'Icon', 'hip' ),
            'type' => Controls_Manager::ICONS,
            'fa4compatibility' => 'icon',
            'skin' => 'inline',
            'label_block' => false,

          ]
        );

        $this->add_control(
          'hip_menu_cta_button_icon_align',
          [
            'label' => esc_html__( 'Icon Position', 'hip' ),
            'type' => Controls_Manager::CHOOSE,
            'default' => 'left',
            'options' => [
              'left' => [
                'title' => esc_html__( 'Left', 'hip' ),
                'icon' => 'eicon-h-align-left',
              ],
              'right' => [
                'title' => esc_html__( 'Right', 'hip' ),
                'icon' => 'eicon-h-align-right',
              ],
            ],

          ]
        );

        $this->add_control(
          'hip_menu_cta_button_icon_indent',
          [
            'label' => esc_html__( 'Icon Spacing', 'hip' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px', 'em', 'rem', 'custom' ],
            'range' => [
              'px' => [
                'max' => 50,
              ],
              'em' => [
                'max' => 5,
              ],
              'rem' => [
                'max' => 5,
              ],
            ],
            'selectors' => [
              '{{WRAPPER}} .hip-menu-button .hip-cta-align-icon-right' => 'margin-left: {{SIZE}}{{UNIT}};',
              '{{WRAPPER}} .hip-menu-button .hip-cta-align-icon-left' => 'margin-right: {{SIZE}}{{UNIT}};',
            ],
          ]
        );

        $this->add_control(
          'hip_menu_cta_button_button_css_id',
          [
            'label' => esc_html__( 'Button ID', 'hip' ),
            'type' => Controls_Manager::TEXT,
            'dynamic' => [
              'active' => true,
            ],
            'ai' => [
              'active' => false,
            ],
            'default' => '',
            'title' => esc_html__( 'Add your custom id WITHOUT the Pound key. e.g: my-id', 'hip' ),
            'description' => sprintf(
              esc_html__( 'Please make sure the ID is unique and not used elsewhere on the page this form is displayed. This field allows %1$sA-z 0-9%2$s & underscore chars without spaces.', 'hip' ),
              '<code>',
              '</code>'
            ),
            'separator' => 'before',
          ]
        );
        $this->end_controls_section();
    }

    /**
     * Register widget style controls
     */
    protected function register_style_controls() {
        $this->__nav_menu_style_controls();
        $this->top_level_nav_style_controls();
        $this->top_level_nav_child_style_controls();
        $this->mega_menu_style_controls();
        $this->hamburger_style_controls();
        $this->contact_lists_style_controls();
        $this->menu_social_icon_style_controls();
        $this->menu_cta_button_style_controls();

    }

    /**
     * @return Main menu styles
     */
    protected function __nav_menu_style_controls() {

        $this->start_controls_section(
          '_section_nav_menu_style_control',
          [
            'label' => __( 'Main Menu', 'hip' ),
            'tab'   => Controls_Manager::TAB_STYLE,
          ]
        );


        $this->add_responsive_control(
          'hip_nav_menu_padding_x',
          array(
            'label'       => __( 'Horizontal Padding', 'uael' ),
            'type'        => Controls_Manager::SLIDER,
            'size_units'  => array( 'px' ),
            'default'     => array(
              'size' => 15,
              'unit' => 'px',
            ),
            'selectors'   => array(
              '{{WRAPPER}} .hip-nav-menu .menu li.menu-item a' => 'padding-left: {{SIZE}}{{UNIT}}; padding-right: {{SIZE}}{{UNIT}}',
            ),
            'render_type' => 'template',
          )
        );

        $this->add_responsive_control(
          'hip_nav_menu_padding_y',
          array(
            'label'       => __( 'Vertical Padding', 'uael' ),
            'type'        => Controls_Manager::SLIDER,
            'size_units'  => array( 'px' ),
            'default'     => array(
              'size' => 15,
              'unit' => 'px',
            ),
            'range'       => array(
              'px' => array(
                'max' => 50,
              ),
            ),
            'selectors'   => array(
              '{{WRAPPER}} .hip-nav-menu .menu li.menu-item a' => 'padding-top: {{SIZE}}{{UNIT}}; padding-bottom: {{SIZE}}{{UNIT}}',
            ),

          )
        );
        $this->add_responsive_control(
          'hip_nav_menu_margin',
          [
            'label'      => __( 'Space Between', 'hip' ),
            'type'       => Controls_Manager::SLIDER,
            'size_units' => ['px', '%'],
            'range'      => [
              'px' => [
                'min'  => 0,
                'max'  => 400,
                'step' => 1,
              ],
              '%'  => [
                'min' => 0,
                'max' => 100,
              ],
            ],
            'default'    => [
              'unit' => 'px',
              'size' => 0,
            ],
            'selectors'  => [
              '{{WRAPPER}} .hip-nav-menu .menu > li.menu-item:not(:last-child)' => 'margin-right: {{SIZE}}{{UNIT}}',
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li.menu-item' => 'margin-top: {{SIZE}}{{UNIT}}; margin-bottom: {{SIZE}}{{UNIT}}',
            ],
          ]
        );
        $this->add_responsive_control(
          'hip_nav_menu_box_top_distance',
          [
            'label'      => __( 'Menu Top Distance From header', 'hip' ),
            'type'       => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%' ],
            'range'      => [
              'px' => [
                'min'  => 0,
                'max'  => 100,
                'step' => 1,
              ],
              '%'  => [
                'min'  => 0,
                'max'  => 100,
                'step' => 1,
              ],
            ],
            'default'    => [
              'unit' => 'px',
              'size' => '60',
            ],
            'selectors'  => [
              '{{WRAPPER}} .hip-navigation-burger-menu .hip-nav-wrapper ' => 'top: {{SIZE}}{{UNIT}};',
            ],
          ]
        );
        $this->add_responsive_control(
          'hip_nav_menu_box_sticky_top_distance',
          [
            'label'      => __( 'Sticky header Top Distance', 'hip' ),
            'type'       => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%' ],
            'range'      => [
              'px' => [
                'min'  => 0,
                'max'  => 100,
                'step' => 1,
              ],
              '%'  => [
                'min'  => 0,
                'max'  => 100,
                'step' => 1,
              ],
            ],
            'default'    => [
              'unit' => 'px',
              'size' => '60',
            ],
            'selectors'  => [
              '{{WRAPPER}} .elementor-sticky--active .hip-navigation-burger-menu .hip-nav-wrapper,
               {{WRAPPER}} .mobile-main-header.fixed-header .hip-navigation-burger-menu .hip-nav-wrapper' => 'top: {{SIZE}}{{UNIT}};',
            ],
          ]
        );
        $this->end_controls_section();

    }
    protected function top_level_nav_style_controls(){
        // Section for Dropdown Menu Styles
        $this->start_controls_section(
          'hip_nav_submenu_section',
          [
            'label' => __('Top Level Menu Styles', 'hip'),
            'tab' => Controls_Manager::TAB_STYLE,
          ]
        );
        $this->add_control(
          '_hip_nav_menu_dropdown_wrap',
          [
            'label' => __( 'Wrapper', 'hip' ),
            'type'  => Controls_Manager::HEADING,
          ]
        );

        $this->add_group_control(
          Group_Control_Background::get_type(),
          [
            'name'     => 'hip_nav_submenu_wrap_background',
            'label'    => __( 'Background', 'hip' ),
            'types'    => ['classic', 'gradient'],
            'exclude'  => ['image'],
            'selector' => '{{WRAPPER}} .hip-navigation-burger-menu .hip-nav-wrapper',
          ]
        );


        /* Items */
        $this->add_control(
          '_hip_nav_menu_dropdown_item_heading',
          [
            'label' => __( 'Items', 'hip' ),
            'type'  => Controls_Manager::HEADING,
            'separator' => 'before',
          ]
        );

        $this->add_responsive_control(
          'hip_nav_submenu_item_padding',
          [
            'label'      => __( 'Padding', 'hip' ),
            'type'       => Controls_Manager::DIMENSIONS,
            'size_units' => ['px', '%'],
            'default'    => [
              'top'    => 15,
              'right'  => '',
              'bottom' => 15,
              'left'   => '',
              'unit'   => 'px',
            ],
            'selectors'  => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container > a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
          ]
        );
// Typography for Normal State
        $this->add_group_control(
          Group_Control_Typography::get_type(),
          [
            'name' => 'hip_nav_top_typography_normal',
            'label' => __('Typography', 'hip'),
            'selector' => '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container > a',
          ]
        );
// Tab Control for Normal, Hover, and Active states
        $this->start_controls_tabs('hip_nav_top_tabs');

// Tab for Normal State
        $this->start_controls_tab('hip_nav_top_tab_normal', [
          'label' => __('Normal', 'hip'),
        ]);


// Color for Normal State
        $this->add_control(
          'hip_nav_top_color_normal',
          [
            'label' => __('Color', 'hip'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container > a,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container > a .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
            ],
          ]
        );

// Background Color for Normal State
        $this->add_control(
          'hip_nav_top_background_normal',
          [
            'label' => __('Background Color', 'hip'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container > a' => 'background-color: {{VALUE}}',
            ],
          ]
        );

        $this->end_controls_tab();

// Tab for Hover State
        $this->start_controls_tab('hip_nav_top_tab_hover', [
          'label' => __('Hover', 'hip'),
        ]);


// Color for Hover State
        $this->add_control(
          'hip_nav_top_color_hover',
          [
            'label' => __('Color', 'hip'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container > a:hover,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container > a:focus,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container > a:hover .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
            ],
          ]
        );

// Background Color for Hover State
        $this->add_control(
          'hip_nav_top_background_hover',
          [
            'label' => __('Background Color', 'hip'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container > a:hover,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container > a:focus,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container.highlighted > a' => 'background-color: {{VALUE}}',
            ],
          ]
        );

        $this->end_controls_tab();

// Tab for Active State
        $this->start_controls_tab('hip_nav_top_tab_active', [
          'label' => __('Active', 'hip'),
        ]);

// Color for Active State
        $this->add_control(
          'hip_nav_top_color_active',
          [
            'label' => __('Color', 'hip'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li.current-menu-item > .hip-nav-item-container > a, 
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li.active > .hip-nav-item-container > a,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li.current-menu-ancestor > .hip-nav-item-container > a,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li.active > .hip-nav-item-container > a .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
            ],
          ]
        );

// Background Color for Active State
        $this->add_control(
          'hip_nav_top_background_active',
          [
            'label' => __('Background Color', 'hip'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li.current-menu-item > .hip-nav-item-container > a, 
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li.active > .hip-nav-item-container > a,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li.current-menu-ancestor > .hip-nav-item-container > a' => 'background-color: {{VALUE}}',
            ],
          ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        $this->add_group_control(
          Group_Control_Box_Shadow::get_type(),
          array(
            'name'      => 'top_level_menu_box_shadow',
//            'exclude'   => array(
//              'box_shadow_position',
//            ),
            'selector' => '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container > a ',
            'fields_options' =>
              [
                'box_shadow_type' =>
                  [
                    'default' =>'yes'
                  ],
                'box_shadow' => [
                  'default' =>
                    [
                      'horizontal' => 0,
                      'vertical' => -1,
                      'blur' => 0,
                      'spread' => 0,
                      'color' => 'rgba(255, 255, 255, 0.12)'
                    ]
                ]
              ],
            'separator' => 'after',
          )
        );
        $this->end_controls_section();

    }
    protected function top_level_nav_child_style_controls() {
    // Section for Dropdown Menu Styles
    $this->start_controls_section(
      'hip_top_level_child_section',
      [
        'label' => __('Top Level Child  Styles', 'hip'),
        'tab' => Controls_Manager::TAB_STYLE,
      ]
    );

    /* Items */
    $this->add_control(
      '_hip_top_level_child_item_heading',
      [
        'label' => __( 'Item', 'hip' ),
        'type'  => Controls_Manager::HEADING,
        'separator' => 'before',
      ]
    );

    $this->add_responsive_control(
      'hip_top_level_child_padding',
      [
        'label'      => __( 'Padding', 'hip' ),
        'type'       => Controls_Manager::DIMENSIONS,
        'size_units' => ['px', '%'],
        'default'    => [
          'top'    => 15,
          'right'  => '',
          'bottom' => 15,
          'left'   => '',
          'unit'   => 'px',
        ],
        'selectors'  => [
          '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
      ]
    );

    // Typography for Normal State
    $this->add_group_control(
      Group_Control_Typography::get_type(),
      [
        'name' => 'hip_top_level_child_typography',
        'label' => __('Typography', 'hip'),
        'selector' => '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a',
      ]
    );

    // Tab Control for Normal, Hover, and Active states
    $this->start_controls_tabs('hip_top_level_child_tabs');

    // Tab for Normal State
    $this->start_controls_tab('hip_top_level_child_tab_normal', [
      'label' => __('Normal', 'hip'),
    ]);

    // Color for Normal State
    $this->add_control(
      'hip_top_level_child_color_normal',
      [
        'label' => __('Color', 'hip'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a,
          {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
        ],
      ]
    );

    // Background Color for Normal State
    $this->add_control(
      'hip_top_level_child_background_normal',
      [
        'label' => __('Background Color', 'hip'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a' => 'background-color: {{VALUE}}',
        ],
      ]
    );

    $this->end_controls_tab();

    // Tab for Hover State
    $this->start_controls_tab('hip_top_level_child_tab_hover', [
      'label' => __('Hover', 'hip'),
    ]);

    // Color for Hover State
    $this->add_control(
      'hip_top_level_child_color_hover',
      [
        'label' => __('Color', 'hip'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:hover,
             {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:focus,
             {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
        ],
      ]
    );

    // Background Color for Hover State
    $this->add_control(
      'hip_top_level_child_background_hover',
      [
        'label' => __('Background Color', 'hip'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:hover,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:focus' => 'background-color: {{VALUE}}',
        ],
      ]
    );

    $this->end_controls_tab();

    // Tab for Active State
    $this->start_controls_tab('hip_top_level_child_tab_active', [
      'label' => __('Active', 'hip'),
    ]);

    // Color for Active State
    $this->add_control(
      'hip_top_level_child_color_active',
      [
        'label' => __('Color', 'hip'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li.current-menu-item > .hip-nav-item-container > a,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li.active > .hip-nav-item-container > a,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li.active > .hip-nav-item-container > a .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
        ],
      ]
    );

    // Background Color for Active State
    $this->add_control(
      'hip_top_level_child_background_active',
      [
        'label' => __('Background Color', 'hip'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
          '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li.current-menu-item > .hip-nav-item-container > a,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li.active > .hip-nav-item-container > a' => 'background-color: {{VALUE}}',
        ],
      ]
    );

    $this->end_controls_tab();
    $this->end_controls_tabs();

    $this->add_group_control(
      Group_Control_Box_Shadow::get_type(),
      [
        'name'      => 'top_level_child_menu_box_shadow',
        'selector' => '{{WRAPPER}} .hip-navigation-burger-menu ul.menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a',
        'fields_options' => [
          'box_shadow_type' => [
            'default' => 'yes'
          ],
          'box_shadow' => [
            'default' => [
              'horizontal' => 0,
              'vertical' => -1,
              'blur' => 0,
              'spread' => 0,
              'color' => 'rgba(255, 255, 255, 0.12)'
            ]
          ]
        ],
        'separator' => 'after',
      ]
    );

    $this->end_controls_section();
}
    protected function mega_menu_style_controls() {
        $this->start_controls_section(
          '_hip_nav_mega_menu_style_control',
          [
            'label' => __( 'Mega Menu', 'hip' ),
            'tab'   => Controls_Manager::TAB_STYLE,
          ]
        );

        $this->add_control(
          'hip_mega_menu_description',
          [
            'raw'             => __( '<b>Note:</b> Please add this class to mega menu Appearance>Menu open the mega menu item and add the class e.g:hip-mega-menu', 'hip' ),
            'type'            => Controls_Manager::RAW_HTML,
            'content_classes' => 'elementor-descriptor',
          ]
        );
        /* Items */
        $this->add_control(
          '_hip_nav_menu_mega_menu_item_main_heading',
          [
            'label' => __( 'Main Item', 'hip' ),
            'type'  => Controls_Manager::HEADING,
            'separator' => 'before',
          ]
        );
        $this->add_control(
          'hip_nav_mega_menu_item_main_color',
          [
            'label'     => __( 'Hover Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#333333',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container  > a:hover,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container.highlighted  > a,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu.current-menu-ancestor > .hip-nav-item-container  > a,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu.current-menu-item  > .hip-nav-item-container  > a' => 'color: {{VALUE}}',
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container  > a:hover .hip-submenu-indicator-wrap,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu.current-menu-ancestor > .hip-nav-item-container  > a .hip-submenu-indicator-wrap,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu.current-menu-item > .hip-nav-item-container  > a .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
            ],
          ]
        );
        $this->add_control(
          'hip_nav_mega_menu_item_main_background_color',
          [
            'label'     => __( 'Hover Background', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#333333',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container.highlighted  > a,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container  > a:hover,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu.current-menu-ancestor > .hip-nav-item-container  > a,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu.current-menu-item > .hip-nav-item-container  > a' => 'background-color: {{VALUE}}',
            ],
            'separator' => 'after',
          ]
        );
        /* Items */
        $this->add_control(
          '_hip_nav_menu_mega_menu_item_heading',
          [
            'label' => __( 'Items', 'hip' ),
            'type'  => Controls_Manager::HEADING,
            'separator' => 'before',
          ]
        );

        $this->add_group_control(
          Group_Control_Typography::get_type(),
          [
            'name'      => 'hip_nav_mega_menu_item_typography',
            'label'     => __( 'Typography', 'hip' ),
            'global' => [
              'default' => Global_Typography::TYPOGRAPHY_PRIMARY,
            ],
            'separator' => 'before',
            'selector'  => '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a',
          ]
        );

        $this->add_responsive_control(
          'hip_nav_mega_menu_item_padding',
          [
            'label'      => __( 'Padding', 'hip' ),
            'type'       => Controls_Manager::DIMENSIONS,
            'size_units' => ['px', '%'],
            'default'    => [
              'top'    => 15,
              'right'  => '',
              'bottom' => 15,
              'left'   => '',
              'unit'   => 'px',
            ],
            'selectors'  => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
          ]
        );

        $this->start_controls_tabs('hip_nav_mega_menu_normal_tabs');

        // Normal tab
        $this->start_controls_tab('hip_nav_mega_menu_normal_tab', [
          'label' => __( 'Normal', 'hip' ),
        ]);

        $this->add_control(
          'hip_nav_mega_menu_item_color',
          [
            'label'     => __( 'Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#333333',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a' => 'color: {{VALUE}}',
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
            ],
          ]
        );
        $this->add_control(
          'hip_nav_mega_menu_item_background_color',
          [
            'label'     => __( 'Background', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#333333',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a' => 'background-color: {{VALUE}}',
            ],
          ]
        );
        $this->end_controls_tab();

        // Hover tab
        $this->start_controls_tab('hip_nav_mega_menu_hover_tab', [
          'label' => __( 'Hover', 'hip' ),
        ]);

        $this->add_control(
          'hip_nav_mega_menu_item_hover_color',
          [
            'label'     => __( 'Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#E2498A',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:hover,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container.highlighted > a,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:focus,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container.highlighted > a .hip-submenu-indicator-wrap,
                 {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:hover .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
            ],
          ]
        );
        $this->add_control(
          'hip_nav_mega_menu_item_hover_background',
          [
            'label'     => __( 'Background Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#E2498A',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:hover,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container.highlighted > a,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:focus' => 'background-color: {{VALUE}}',
            ],
          ]
        );
        $this->end_controls_tab();

        // Active tab
        $this->start_controls_tab('hip_nav_mega_menu_active_tab', [
          'label' => __( 'Active', 'hip' ),
        ]);

        $this->add_control(
          'hip_nav_mega_menu_item_active_color',
          [
            'label'     => __( 'Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#E2498A',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li.current-menu-item > .hip-nav-item-container > a,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li.active > .hip-nav-item-container > a,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li.active > .hip-nav-item-container > a .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
            ],
          ]
        );
        $this->add_control(
          'hip_nav_mega_menu_item_active_background',
          [
            'label'     => __( 'Background Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#E2498A',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li.current-menu-item > .hip-nav-item-container > a,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li.active > .hip-nav-item-container > a' => 'background-color: {{VALUE}}',
            ],
          ]
        );
        $this->end_controls_tab();
        $this->end_controls_tabs();

        $this->add_group_control(
          Group_Control_Box_Shadow::get_type(),
          [
            'name'      => 'mega_menu_menu_box_shadow',
            'selector'  => '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a',
            'fields_options' => [
              'box_shadow_type' => [
                'default' => 'yes'
              ],
              'box_shadow' => [
                'default' => [
                  'horizontal' => 0,
                  'vertical' => -1,
                  'blur' => 0,
                  'spread' => 0,
                  'color' => 'rgba(255, 255, 255, 0.12)'
                ]
              ]
            ],
            'separator' => 'after',
          ]
        );

        // First level child styles
        $this->mega_level_nav_child_style_controls();
        $this->end_controls_section();
    }
    protected function mega_level_nav_child_style_controls() {
        /* Items */
        $this->add_control(
          '_hip_nav_mega_menu_item_child_heading',
          [
            'label' => __( 'Items Children', 'hip' ),
            'type' => Controls_Manager::HEADING,
            'separator' => 'before',
          ]
        );

        $this->add_group_control(
          Group_Control_Typography::get_type(),
          [
            'name' => 'hip_nav_mega_menu_item_child_typography',
            'label' => __( 'Typography', 'hip' ),
            'global' => [
              'default' => Global_Typography::TYPOGRAPHY_PRIMARY,
            ],
            'separator' => 'before',
            'selector' => '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a',
          ]
        );

        $this->add_responsive_control(
          'hip_nav_mega_menu_item_child_padding',
          [
            'label' => __( 'Padding', 'hip' ),
            'type' => Controls_Manager::DIMENSIONS,
            'size_units' => ['px', '%'],
            'default' => [
              'top' => 15,
              'right' => '',
              'bottom' => 15,
              'left' => '',
              'unit' => 'px',
            ],
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
          ]
        );

        $this->start_controls_tabs('hip_nav_mega_menu_item_child_normal_tabs');

        $this->start_controls_tab(
          'hip_nav_mega_menu_item_child_normal_tab',
          [
            'label' => __( 'Normal', 'hip' ),
          ]
        );

        $this->add_control(
          'hip_nav_mega_menu_item_child_color',
          [
            'label' => __( 'Color', 'hip' ),
            'type' => Controls_Manager::COLOR,
            'default' => '#333333',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a' => 'color: {{VALUE}}',
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
            ],
          ]
        );

        $this->add_control(
          'hip_nav_mega_menu_item_child_normal_background',
          [
            'label'     => __( 'Background Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#E2498A',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a' => 'background-color: {{VALUE}}',
            ],
          ]
        );
        $this->end_controls_tab();

        // Hover color
        $this->start_controls_tab(
          'hip_nav_mega_menu_item_child_hover_tab',
          [
            'label' => __( 'Hover', 'hip' ),
          ]
        );

        $this->add_control(
          'hip_nav_mega_menu_item_child_hover_color',
          [
            'label' => __( 'Color', 'hip' ),
            'type' => Controls_Manager::COLOR,
            'default' => '#E2498A',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container.highlighted > a,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:hover,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:focus' => 'color: {{VALUE}}',
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container.highlighted > a .hip-submenu-indicator-wrap,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:hover .hip-submenu-indicator-wrap,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:focus .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
            ],
          ]
        );

        $this->add_control(
          'hip_nav_mega_menu_item_child_hover_background',
          [
            'label'     => __( 'Background Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#E2498A',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container.highlighted > a,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:hover,
              {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a:focus' => 'background-color: {{VALUE}}',
            ],
          ]
        );
        $this->end_controls_tab();

        // Active color
        $this->start_controls_tab(
          'hip_nav_mega_menu_item_child_active_tab',
          [
            'label' => __( 'Active', 'hip' ),
          ]
        );

        $this->add_control(
          'hip_nav_mega_menu_item_child_active_color',
          [
            'label' => __( 'Color', 'hip' ),
            'type' => Controls_Manager::COLOR,
            'default' => '#E2498A',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + ul.hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li.active > .hip-nav-item-container > a,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + ul.hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li.current-menu-item > .hip-nav-item-container > a' => 'color: {{VALUE}}',
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + ul.hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a .hip-submenu-indicator-wrap' => 'color: {{VALUE}}',
            ],
          ]
        );

        $this->add_control(
          'hip_nav_mega_menu_item_child_active_background',
          [
            'label'     => __( 'Background Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#E2498A',
            'selectors' => [
              '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + ul.hip-sub-menu > li > .hip-nav-item-container + ul.hip-sub-menu > li.active > .hip-nav-item-container > a,
                {{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + ul.hip-sub-menu > li > .hip-nav-item-container + ul.hip-sub-menu > li.current-menu-item > .hip-nav-item-container > a' => 'background-color: {{VALUE}}',
            ],
          ]
        );
        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_group_control(
          Group_Control_Box_Shadow::get_type(),
          [
            'name' => '_mega_menu_item_child_menu_box_shadow',
            'selector' => '{{WRAPPER}} .hip-navigation-burger-menu ul.menu li.hip-mega-menu > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container + .hip-sub-menu > li > .hip-nav-item-container > a',
            'fields_options' => [
              'box_shadow_type' => [
                'default' => 'yes',
              ],
              'box_shadow' => [
                'default' => [
                  'horizontal' => 0,
                  'vertical' => -1,
                  'blur' => 0,
                  'spread' => 0,
                  'color' => 'rgba(255, 255, 255, 0.12)',
                ],
              ],
            ],
            'separator' => 'after',
          ]
        );
    }
    protected function hamburger_style_controls() {
        $this->start_controls_section(
          'hip_hamburger_icon_style_control',
          [
            'label' => __( 'Hamburger Icon', 'hip' ),
            'tab'   => Controls_Manager::TAB_STYLE,
          ]
        );


        $this->add_responsive_control(
          'hip_hamburger_icon_box_width',
          [
            'label'      => __( 'Box Width', 'hip' ),
            'type'       => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%' ],
            'range'      => [
              'px' => [
                'min'  => 0,
                'max'  => 150,
                'step' => 1,
              ],
              '%'  => [
                'min'  => 0,
                'max'  => 100,
                'step' => 1,
              ],
            ],
            'default'    => [
              'unit' => 'px',
              'size' => 32,
            ],
            'selectors'  => [
              '{{WRAPPER}}  .hip-menu-toggler' => 'width: {{SIZE}}{{UNIT}};',
            ],
          ]
        );

        $this->add_responsive_control(
          'hip_hamburger_icon_box_height',
          [
            'label'      => __( 'Box Height', 'hip' ),
            'type'       => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%' ],
            'range'      => [
              'px' => [
                'min'  => 0,
                'max'  => 150,
                'step' => 1,
              ],
              '%'  => [
                'min'  => 0,
                'max'  => 100,
                'step' => 1,
              ],
            ],
            'default'    => [
              'unit' => 'px',
              'size' => 28,
            ],
            'selectors'  => [
              '{{WRAPPER}}  .hip-menu-toggler' => 'height: {{SIZE}}{{UNIT}};',
            ],
          ]
        );

        $this->add_responsive_control(
          'hip_hamburger_icon_border_width',
          [
            'label'      => __( 'Box Border Width', 'hip' ),
            'type'       => Controls_Manager::SLIDER,
            'size_units' => [ 'px' ],
            'range'      => [
              'px' => [
                'min'  => 0,
                'max'  => 10,
                'step' => 1,
              ],
            ],
            'default'    => [
              'unit' => 'px',
              'size' => 1,
            ],
            'selectors'  => [
              '{{WRAPPER}}  .hip-menu-toggler' => 'border-width: {{SIZE}}{{UNIT}};',
            ],
          ]
        );

        $this->add_responsive_control(
          'hip_hamburger_icon_border_padding',
          [
            'label'      => __( 'Box Padding', 'hip' ),
            'type'       => Controls_Manager::DIMENSIONS,
            'size_units' => ['px', '%'],
            'default'    => [
              'top'    => '',
              'right'  => '',
              'bottom' => '',
              'left'   => '',
              'unit'   => 'px',
            ],
            'selectors'  => [
              '{{WRAPPER}}  .hip-menu-toggler' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
          ]
        );

        $this->add_control(
          'hip_hamburger_icon_box_border_radius',
          [
            'label'      => __( 'Border Radius', 'hip' ),
            'type'       => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%' ],
            'range'      => [
              'px' => [
                'min'  => 0,
                'max'  => 100,
                'step' => 1,
              ],
            ],
            'default'    => [
              'unit' => 'px',
              'size' => 5,
            ],
            'selectors'  => [
              '{{WRAPPER}}  .hip-menu-toggler' => 'border-radius: {{SIZE}}{{UNIT}};',
            ],
          ]
        );

        $this->add_responsive_control(
          'hip_hamburger_icon_lines_height',
          [
            'label'      => __( 'Lines Height', 'hip' ),
            'type'       => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%' ],
            'range'      => [
              'px' => [
                'min'  => 0,
                'max'  => 150,
                'step' => 1,
              ],
              '%'  => [
                'min'  => 0,
                'max'  => 100,
                'step' => 1,
              ],
            ],
            'default'    => [
              'unit' => 'px',
              'size' => 5,
            ],
            'selectors'  => [
              '{{WRAPPER}}  .hip-menu-toggler .hip-nav-toggle-line,
                {{WRAPPER}}  .hip-menu-toggler:before, 
                {{WRAPPER}}  .hip-menu-toggler:after' => 'height: {{SIZE}}{{UNIT}};',
            ],
          ]
        );

        $this->add_control(
          'hip_hamburger_icon_lines_border_radius',
          [
            'label'      => __( 'Lines Border Radius', 'hip' ),
            'type'       => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%' ],
            'range'      => [
              'px' => [
                'min'  => 0,
                'max'  => 100,
                'step' => 1,
              ],
            ],
            'default'    => [
              'unit' => 'px',
              'size' => 5,
            ],
            'selectors'  => [
              '{{WRAPPER}}  .hip-menu-toggler .hip-nav-toggle-line,
                {{WRAPPER}} .hip-menu-toggler:before, 
                {{WRAPPER}} .hip-menu-toggler:after' => 'border-radius: {{SIZE}}{{UNIT}};',
            ],
          ]
        );

        $this->add_control(
          'hip_hamburger_icon_lines_color',
          [
            'label'     => __( 'Lines Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#000',
            'selectors' => [
              '{{WRAPPER}}   .hip-menu-toggler .hip-nav-toggle-line,
                {{WRAPPER}}   .hip-menu-toggler:before, 
                {{WRAPPER}}   .hip-menu-toggler:after' => 'background-color: {{VALUE}}',
            ],
          ]
        );

        $this->add_control(
          'hip_hamburger_icon_border_color',
          [
            'label'     => __( 'Sticky Lines Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#fff',
           'selectors' => [
              '{{WRAPPER}}  .mobile-main-header.fixed-header   .hip-menu-toggler .hip-nav-toggle-line,
                {{WRAPPER}}   .mobile-main-header.fixed-header  .hip-menu-toggler:before, 
                {{WRAPPER}}   .mobile-main-header.fixed-header  .hip-menu-toggler:after' => 'background-color: {{VALUE}}',
            ],
          ]
        );

        $this->end_controls_section();
    }
    protected function contact_lists_style_controls() {
        $this->start_controls_section(
          'hip_contact_lists_style_control',
          [
            'label' => __( 'Contact Lists Style', 'hip' ),
            'tab'   => Controls_Manager::TAB_STYLE,
            'condition' => [
              'enabled_menu_contact_list' => 'yes',
            ],
          ]
        );



        $this->add_responsive_control(
          'hip_contact_list_item_padding',
          [
            'label'      => __( 'Item Padding', 'hip' ),
            'type'       => Controls_Manager::DIMENSIONS,
            'size_units' => ['px', '%'],
            'default'    => [
              'top'    => '',
              'right'  => '40',
              'bottom' => '',
              'left'   => '40',
              'unit'   => 'px',
            ],
            'selectors'  => [
              '{{WRAPPER}} ul.hip-nav-contact-lists' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
          ]
        );

        $this->add_responsive_control(
          'hip_contact_list_item_space_between',
          [
            'label' => esc_html__( 'Space Between Items', 'hip' ),
            'type' => Controls_Manager::SLIDER,
            'range' => [
              'px' => [
                'min' => 1,
                'max' => 20,
              ],
            ],
            'default' => [
              'size' => 18,
            ],
            'selectors' => [
              '{{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list:not(:last-child)' => 'margin-bottom: {{SIZE}}{{UNIT}};',
            ],
          ]
        );

    //icon Controls
        $this->add_control(
          'hip_contact_list_icon',
          [
            'label' => __( 'Icon', 'hip' ),
            'type'  => Controls_Manager::HEADING,
            'separator' => 'before',
          ]
        );
        $this->start_controls_tabs( 'icon_colors' );

        $this->start_controls_tab(
          'hip_contact_list_colors_normal',
          [
            'label' => esc_html__( 'Normal', 'hip' ),
          ]
        );

        $this->add_control(
          'hip_contact_list_icon_color',
          [
            'label' => esc_html__( 'Color', 'hip' ),
            'type' => Controls_Manager::COLOR,
            'default'   => '#000',
            'selectors' => [
              '{{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list a .hip-contact-list-icon i' => 'color: {{VALUE}};',
              '{{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list a .hip-contact-list-icon  svg' => 'fill: {{VALUE}};',
            ],

          ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
          'hip_contact_list_icon_colors_hover',
          [
            'label' => esc_html__( 'Hover', 'hip' ),
          ]
        );

        $this->add_control(
          'hip_contact_list_icon_color_hover',
          [
            'label' => esc_html__( 'Color', 'hip' ),
            'type' => Controls_Manager::COLOR,
            'default' => '',
            'selectors' => [
              '{{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list a:hover .hip-contact-list-icon i,
              {{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list a:focus .hip-contact-list-icon i,
              {{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list.current-menu-item a .hip-contact-list-icon i' => 'color: {{VALUE}};',
              '{{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list a:hover .hip-contact-list-icon  svg,
              {{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list a:focus .hip-contact-list-icon  svg,
              {{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list.current-menu-item a .hip-contact-list-icon  svg' => 'fill: {{VALUE}};',
            ],
          ]
        );
        $this->end_controls_tab();

        $this->end_controls_tabs();
        $this->add_responsive_control(
          'hip_contact_list_icon_size',
          [
            'label' => esc_html__( 'Size', 'hip' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%', 'em', 'rem', 'vw', 'custom' ],
            'default' => [
              'size' => 16,
            ],
            'range' => [
              'px' => [
                'min' => 6,
              ],
              '%' => [
                'min' => 6,
              ],
              'vw' => [
                'min' => 6,
              ],
            ],
            'separator' => 'before',
            'selectors' => [
              '{{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list .hip-contact-list-icon svg' => 'width: {{SIZE}}{{UNIT}};',
              '{{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list .hip-contact-list-icon i' => 'font-size: {{SIZE}}{{UNIT}};',
            ],
          ]
        );
        $this->add_responsive_control(
          'hip_contact_list_text_indent',
          [
            'label' => esc_html__( 'Gap', 'hip' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%', 'em', 'rem', 'vw', 'custom' ],
            'range' => [
              'px' => [
                'max' => 50,
              ],
            ],
            'separator' => 'after',
            'selectors' => [
              '{{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list .hip-contact-list-icon svg,
              {{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list .hip-contact-list-icon i' =>  'margin-right: {{SIZE}}{{UNIT}};',
            ],
          ]
        );


//        list item text controls
        $this->add_control(
          'hip_contact_list_text',
          [
            'label' => __( 'Text', 'hip' ),
            'type'  => Controls_Manager::HEADING,
            'separator' => 'before',
          ]
        );
        $this->add_group_control(
          Group_Control_Typography::get_type(),
          [
            'name' => 'hip_contact_list_text',
            'label' => __('Typography', 'hip'),
            'selector' => '{{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list a',
          ]
        );
        $this->start_controls_tabs( 'list_text_colors' );

        $this->start_controls_tab(
          'hip_contact_list_text_colors_normal',
          [
            'label' => esc_html__( 'Normal', 'hip' ),
          ]
        );
        $this->add_control(
          'hip_contact_list_item_text_color',
          [
            'label'     => __( 'Text Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#000',
            'selectors' => [
              '{{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list a .hip-nav-contact-list-text' => 'color: {{VALUE}}',
            ],
          ]
        );
        $this->end_controls_tab();
        $this->start_controls_tab(
          'hip_contact_list_text_colors_hover',
          [
            'label' => esc_html__( 'Hover', 'hip' ),
          ]
        );
        $this->add_control(
          'hip_contact_list_item_text_color_hover',
          [
            'label'     => __( 'Text Color', 'hip' ),
            'type'      => Controls_Manager::COLOR,
            'default'   => '#000',
            'selectors' => [
              '{{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list a:hover .hip-nav-contact-list-text,
              {{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list a:focus .hip-nav-contact-list-text
              {{WRAPPER}} .hip-nav-contact-lists .hip-nav-contact-list.current-menu-item a .hip-nav-contact-list-text' => 'color: {{VALUE}}',
            ],
          ]
        );
        $this->end_controls_tab();
        $this->end_controls_tabs();
        $this->end_controls_section();
    }
    protected function menu_social_icon_style_controls() {
        $this->start_controls_section(
          'hip_menu_social_section_icon_list',
          [
            'label' => esc_html__( 'Social Icons', 'hip' ),
            'tab' => Controls_Manager::TAB_STYLE,
            'condition' => [
              'enabled_menu_social_icons' => 'yes',
            ],
          ]
        );
        $this->add_responsive_control(
          'hip_menu_social_item_padding',
          [
            'label'      => __( 'Wrapper Padding', 'hip' ),
            'type'       => Controls_Manager::DIMENSIONS,
            'size_units' => ['px', '%'],
            'default'    => [
              'top'    => '',
              'right'  => '40',
              'bottom' => '',
              'left'   => '40',
              'unit'   => 'px',
            ],
            'selectors'  => [
              '{{WRAPPER}} ul.hip-nav-social-icons ' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
          ]
        );
        $this->add_responsive_control(
          'hip_menu_social_icon_size',
          [
            'label' => esc_html__( 'Icon Size', 'hip' ),
            'type' => Controls_Manager::SLIDER,
            'range' => [
              'px' => [
                'min' => 1,
                'max' => 20,
              ],
            ],
            'default' => [
              'size' => 18,
            ],
            'selectors' => [
              '{{WRAPPER}} ul.hip-nav-social-icons li svg' => 'width: {{SIZE}}{{UNIT}};',
              '{{WRAPPER}} ul.hip-nav-social-icons li i' => 'font-size: {{SIZE}}{{UNIT}};',
            ],
          ]
        );
        $this->add_responsive_control(
          'hip_menu_social_space_between',
          [
            'label' => esc_html__( 'Space Between', 'hip' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px', 'em', 'rem', 'custom' ],
            'range' => [
              'px' => [
                'max' => 50,
              ],
            ],
            'selectors' => [
              '{{WRAPPER}} ul.hip-nav-social-icons li' => 'margin-right: calc({{SIZE}}{{UNIT}}/2); margin-left: calc({{SIZE}}{{UNIT}}/2)',
              '{{WRAPPER}} ul.hip-nav-social-icons ' => 'margin-right: calc(-{{SIZE}}{{UNIT}}/2); margin-left: calc(-{{SIZE}}{{UNIT}}/2)',
              ' {{WRAPPER}} ul.hip-nav-social-icons li:after' => 'right: calc(-{{SIZE}}{{UNIT}}/2)',

            ],
          ]
        );

        $this->add_control(
          'hip_menu_social_divider',
          [
            'label' => esc_html__( 'Divider', 'hip' ),
            'type' => Controls_Manager::SWITCHER,
            'label_off' => esc_html__( 'Off', 'hip' ),
            'label_on' => esc_html__( 'On', 'hip' ),
            'selectors' => [
              '{{WRAPPER}} ul.hip-nav-social-icons li:not(:last-child):after' => 'content: ""',
            ],
            'separator' => 'before',
          ]
        );

        $this->add_control(
          'hip_menu_social_divider_style',
          [
            'label' => esc_html__( 'Style', 'hip' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
              'solid' => esc_html__( 'Solid', 'hip' ),
              'double' => esc_html__( 'Double', 'hip' ),
              'dotted' => esc_html__( 'Dotted', 'hip' ),
              'dashed' => esc_html__( 'Dashed', 'hip' ),
            ],
            'default' => 'solid',
            'condition' => [
              'hip_menu_social_divider' => 'yes',
            ],
            'selectors' => [
              '{{WRAPPER}} ul.hip-nav-social-icons li:not(:last-child):after' => 'border-top-style: {{VALUE}}',
              '{{WRAPPER}} ul.hip-nav-social-icons li:not(:last-child):after' => 'border-left-style: {{VALUE}}',
            ],
          ]
        );

        $this->add_control(
          'hip_menu_social_divider_weight',
          [
            'label' => esc_html__( 'Weight', 'hip' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px', 'em', 'rem', 'custom' ],
            'default' => [
              'size' => 1,
            ],
            'range' => [
              'px' => [
                'min' => 1,
                'max' => 20,
              ],
            ],
            'condition' => [
              'hip_menu_social_divider' => 'yes',
            ],
            'selectors' => [
              '{{WRAPPER}} ul.hip-nav-social-icons li:not(:last-child):after' => 'border-top-width: {{SIZE}}{{UNIT}}',
              '{{WRAPPER}} ul.hip-nav-social-icons li:not(:last-child):after' => 'border-left-width: {{SIZE}}{{UNIT}}',

            ],
          ]
        );

        $this->add_control(
          'hip_menu_social_divider_width',
          [
            'label' => esc_html__( 'Width', 'hip' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%', 'em', 'rem', 'vw', 'custom' ],
            'default' => [
              'unit' => '%',
            ],
            'condition' => [
              'hip_menu_social_divider' => 'yes',

            ],
            'selectors' => [
              '{{WRAPPER}} ul.hip-nav-social-icons li:not(:last-child):after' => 'width: {{SIZE}}{{UNIT}}',
            ],
          ]
        );

        $this->add_control(
          'hip_menu_social_divider_height',
          [
            'label' => esc_html__( 'Height', 'hip' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%', 'em', 'rem', 'vh', 'custom' ],
            'default' => [
              'unit' => '%',
            ],
            'range' => [
              'px' => [
                'min' => 1,
                'max' => 100,
              ],
              '%' => [
                'min' => 1,
                'max' => 100,
              ],
              'vh' => [
                'min' => 1,
                'max' => 100,
              ],
            ],
            'condition' => [
              'hip_menu_social_divider' => 'yes',

            ],
            'selectors' => [
              '{{WRAPPER}} ul.hip-nav-social-icons li:not(:last-child):after' => 'height: {{SIZE}}{{UNIT}}',
            ],
          ]
        );

        $this->add_control(
          'hip_menu_social_divider_color',
          [
            'label' => esc_html__( 'Color', 'hip' ),
            'type' => Controls_Manager::COLOR,
            'default' => '#ddd',
            'condition' => [
              'hip_menu_social_divider' => 'yes',
            ],
            'selectors' => [
              '{{WRAPPER}} ul.hip-nav-social-icons li:not(:last-child):after' => 'border-color: {{VALUE}}',
            ],
          ]
        );
        //icon Controls
        $this->add_control(
          'hip_menu_social_icon',
          [
            'label' => __( 'Icon', 'hip' ),
            'type'  => Controls_Manager::HEADING,
            'separator' => 'before',
          ]
        );
        $this->start_controls_tabs( 'hip_menu_social_icon_colors' );

        $this->start_controls_tab(
          'hip_menu_social_icon_colors_normal',
          [
            'label' => esc_html__( 'Normal', 'hip' ),
          ]
        );

        $this->add_control(
          'hip_menu_social_icon_color',
          [
            'label' => esc_html__( 'Color', 'hip' ),
            'type' => Controls_Manager::COLOR,
            'default'   => '#000',
            'selectors' => [
              '{{WRAPPER}} ul.hip-nav-social-icons li a  i' => 'color: {{VALUE}};',
              '{{WRAPPER}} ul.hip-nav-social-icons li a  svg' => 'fill: {{VALUE}};',
            ],

          ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
          'hip_menu_social_icon_colors_hover',
          [
            'label' => esc_html__( 'Hover', 'hip' ),
          ]
        );

        $this->add_control(
          'hip_menu_social_icon_color_hover',
          [
            'label' => esc_html__( 'Color', 'hip' ),
            'type' => Controls_Manager::COLOR,
            'default' => '',
            'selectors' => [
              '{{WRAPPER}} ul.hip-nav-social-icons li a:hover  i,
              {{WRAPPER}} ul.hip-nav-social-icons li a:focus  i' => 'color: {{VALUE}};',
              '{{WRAPPER}} ul.hip-nav-social-icons li a:hover  svg,
              {{WRAPPER}} ul.hip-nav-social-icons li a:focus  svg' => 'fill: {{VALUE}};',
            ],
          ]
        );
        $this->end_controls_tab();

        $this->end_controls_tabs();
        $this->end_controls_section();
    }
    protected function menu_cta_button_style_controls() {
        $this->start_controls_section(
          'hip_menu_cta_button_style',
          [
            'label' => esc_html__( 'CTA Button', 'hip' ),
            'tab' => Controls_Manager::TAB_STYLE,
            'condition' => [
              'enabled_menu_cta_button' => 'yes',
            ],
          ]
        );

        $this->add_group_control(
          Group_Control_Typography::get_type(),
          [
            'name'      => 'hip_nav_cta_button_typography',
            'label'     => __( 'Typography', 'hip' ),
            'global' => [
              'default' => Global_Typography::TYPOGRAPHY_PRIMARY,
            ],
            'separator' => 'before',
            'selector' => '{{WRAPPER}} .hip-menu-button',
          ]
        );
        $this->start_controls_tabs( 'hip_menu_tabs_button_style', [
        ] );

        $this->start_controls_tab(
          'hip_menu_tab_button_normal',
          [
            'label' => esc_html__( 'Normal', 'hip' ),
          ]
        );

        $this->add_control(
          'hip_menu_button_text_color',
          [
            'label' => esc_html__( 'Text Color', 'hip' ),
            'type' => Controls_Manager::COLOR,
            'default' => '',
            'selectors' => [
              '{{WRAPPER}} .hip-menu-button' => 'fill: {{VALUE}}; color: {{VALUE}};',
            ],

          ]
        );

        $this->add_group_control(
          Group_Control_Background::get_type(),
          [
            'name' => 'background',
            'types' => [ 'classic', 'gradient' ],
            'exclude' => [ 'image' ],
            'selector' => '{{WRAPPER}} .hip-menu-button',
            'fields_options' => [
              'background' => [
                'default' => 'classic',
              ],
            ],

          ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
          'hip_menu_tab_button_hover',
          [
            'label' => esc_html__( 'Hover', 'hip' ),
          ]
        );

        $this->add_control(
          'hip_menu_hover_color',
          [
            'label' => esc_html__( 'Text Color', 'hip' ),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
              '{{WRAPPER}} .hip-menu-button:hover, {{WRAPPER}} .hip-menu-button:focus' => 'color: {{VALUE}};',
              '{{WRAPPER}} .hip-menu-button:hover svg, {{WRAPPER}} .hip-menu-button:focus svg' => 'fill: {{VALUE}};',
            ],

          ]
        );

        $this->add_group_control(
          Group_Control_Background::get_type(),
          [
            'name' => 'hip_menu_button_background_hover',
            'types' => [ 'classic', 'gradient' ],
            'exclude' => [ 'image' ],
            'selector' => '{{WRAPPER}} .hip-menu-button:hover, {{WRAPPER}} .hip-menu-button:focus',
            'fields_options' => [
              'background' => [
                'default' => 'classic',
              ],
            ],
          ]
        );

        $this->add_control(
          'hip_menu_button_hover_border_color',
          [
            'label' => esc_html__( 'Border Color', 'hip' ),
            'type' => Controls_Manager::COLOR,
            'condition' => [
              'border_border!' => '',
            ],
            'selectors' => [
              '{{WRAPPER}} .hip-menu-button:hover, {{WRAPPER}} .hip-menu-button:focus' => 'border-color: {{VALUE}};',
            ],
          ]
        );

        $this->add_control(
          'hip_menu_button_hover_transition_duration',
          [
            'label' => esc_html__( 'Transition Duration', 'hip' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 's', 'ms', 'custom' ],
            'default' => [
              'unit' => 's',
            ],
            'selectors' => [
              '{{WRAPPER}} .hip-menu-button' => 'transition-duration: {{SIZE}}{{UNIT}};',
            ],
          ]
        );

        $this->add_control(
          'hip_menu_hover_animation',
          [
            'label' => esc_html__( 'Hover Animation', 'hip' ),
            'type' => Controls_Manager::HOVER_ANIMATION,

          ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();
        $this->add_group_control(
          Group_Control_Border::get_type(),
          [
            'name' => 'hip-menu-button-border',
            'selector' => '{{WRAPPER}} .hip-menu-button',
            'separator' => 'before',
          ]
        );

        $this->add_responsive_control(
          'hip_menu_border_radius',
          [
            'label' => esc_html__( 'Border Radius', 'hip' ),
            'type' => Controls_Manager::DIMENSIONS,
            'size_units' => [ 'px', '%', 'em', 'rem', 'custom' ],
            'selectors' => [
              '{{WRAPPER}} .hip-menu-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
          ]
        );

        $this->add_group_control(
          Group_Control_Box_Shadow::get_type(),
          [
            'name' => 'hip_menu_button_box_shadow',
            'selector' => '{{WRAPPER}} .hip-menu-button
            ',
          ]
        );
        $this->add_responsive_control(
          'hip_button_margin',
          [
            'label' => esc_html__( 'Margin', 'hip' ),
            'type' => Controls_Manager::DIMENSIONS,
            'size_units' => [ 'px', '%', 'em', 'rem', 'vw', 'custom' ],
            'selectors' => [
              '{{WRAPPER}} .hip-menu-button-wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
            'separator' => 'before',
          ]
        );
        $this->add_responsive_control(
          'hip_menu_text_padding',
          [
            'label' => esc_html__( 'Padding', 'hip' ),
            'type' => Controls_Manager::DIMENSIONS,
            'size_units' => [ 'px', '%', 'em', 'rem', 'vw', 'custom' ],
            'selectors' => [
              '{{WRAPPER}} .hip-menu-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
            'separator' => 'before',
          ]
        );

        $this->end_controls_section();
    }
    protected function render() {
        $settings = $this->get_settings_for_display();
        $hamburger_icon_style = $settings['hip_hamburger_icon_styles'];
        $prefix_class = 'hip-hamburger-toggle-' . $hamburger_icon_style;

        // Placeholder message based on menu existence
        if ($this->get_menus()) {
            $placeholder_msg = __( 'Please Select a Menu.', 'hip' );
        } else {
            $placeholder_msg = __( 'You don\'t have any menu created. Please create a new one from Here', 'hip' );
        }

        // If no menu is selected, show the placeholder
        if (empty($settings['hip_nav_menu_list'])) {
            ?>
            <div class="hip-editor-placeholder">
                <h4 class="hip-editor-placeholder-title">
                    <?php esc_html_e( 'Navigation Menu', 'hip' ); ?>
                </h4>
                <div class="hip-editor-placeholder-content">
                    <?php echo esc_html($placeholder_msg); ?>
                </div>
            </div>
            <?php
        } else {
            // Check if the selected menu has items

            $menu_items = wp_get_nav_menu_items($settings['hip_nav_menu_list']);
            if ($menu_items !== false && count($menu_items) > 0) {
                ?>
                <nav class="hip-nav-menu hip-navigation-burger-menu">
                    <div class="hip-nav-humberger-wrapper">
                        <div class="hip-menu-open-icon hip-menu-toggler <?php echo esc_attr($prefix_class); ?>" data-humberger="open">
                            <span class="hip-nav-toggle-line"></span>
                        </div>
                    </div>
                    <?php


                    $this->render_raw();


                    ?>
                </nav>
                <?php
            }
        }
    }

    protected function render_raw() {
        $settings = $this->get_settings_for_display();
        $walker = ( class_exists( 'Hip_Nav_Menu_Walker' ) ? new \Hip_Nav_Menu_Walker() : '' );

        ?>

        <div class="hip-nav-wrapper" style="visibility:hidden;width:0;">
            <div class="hip-menu-inner">
            <?php
            $args = array(
                'items_wrap'    =>  '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'menu'          => $settings['hip_nav_menu_list'],
                'container'     => '',
                'fallback_cb'   => false,
                'depth'         => 4,
                'link_before'   => '<span class="menu-item-title">',
                'link_after'    => '</span>',
                'walker'        => $walker,
                'sub_indicator' => '<span class="hip-navigation-submenu-indicator"></span>',
            );

            wp_nav_menu( $args );
              $this->nav_contact_list_render();
              $this->hip_menu_cta_button_render();
              $this->nav_social_icons_render();
            ?>
           </div>
        </div>
        <?php
    }
    protected function nav_contact_list_render() {
            global $wp;
            $settings = $this->get_settings_for_display();
            $fallback_defaults = [
              'fa fa-check',
              'fa fa-times',
              'fa fa-dot-circle-o',
            ];
            $enable_contact_list = $settings['enabled_menu_contact_list'] ? $settings['hip_nav_contact_icon_list'] : "";
            $nav_contact_list = $settings['hip_nav_contact_icon_list'] ? $settings['hip_nav_contact_icon_list'] : "";
            $migration_allowed = Icons_Manager::is_migration_allowed();

            if ($enable_contact_list) { ?>
                <ul class="hip-nav-contact-lists">
                    <?php
                    $current_page_url = trailingslashit(home_url(add_query_arg(array(),$wp->request))); // Get the current page URL with trailing slash

                    foreach ($nav_contact_list as $index => $item) {
                        $repeater_setting_key = $this->get_repeater_setting_key('hip_nav_list_text', 'hip_nav_contact_icon_list', $index);
                        $this->add_render_attribute($repeater_setting_key, 'class', 'hip-nav-contact-list-text');
                        $this->add_inline_editing_attributes($repeater_setting_key);
                        $id = $item['hip_nav_list_id'] ? $item['hip_nav_list_id'] : "";

                        // Get the URL of the current item and ensure it has a trailing slash
                        $item_url = !empty($item['hip_nav_menu_link']['url']) ? trailingslashit($item['hip_nav_menu_link']['url']) : '';

                        // Convert relative URL to absolute URL
                        if (strpos($item_url, home_url()) !== 0) {
                            $item_url = home_url($item_url);
                        }

                        // Check if the item URL matches the URL of the current page
                        $current_page_class = ($current_page_url === $item_url) ? 'current-menu-item' : '';
                        ?>
                        <li class="hip-nav-contact-list menu-item <?php echo $current_page_class; ?>" <?php if (!empty($id)) { echo 'id="' . esc_attr($id) . '"'; } ?>>
                            <?php if (!empty($item['hip_nav_menu_link']['url'])) {
                            $link_key = 'link_' . $index;
                            $this->add_link_attributes($link_key, $item['hip_nav_menu_link']); ?>
                            <a <?php $this->print_render_attribute_string($link_key); ?>>
                                <?php } ?>

                                <?php
                                if (!isset($item['icon']) && !$migration_allowed) {
                                    $item['icon'] = isset($fallback_defaults[$index]) ? $fallback_defaults[$index] : 'fa fa-check';
                                }
                                $migrated = isset($item['__fa4_migrated']['hip_nav_selected_icon']);
                                $is_new = !isset($item['icon']) && $migration_allowed;
                                if (!empty($item['icon']) || (!empty($item['hip_nav_selected_icon']['value']) && $is_new)) { ?>
                                    <span class="hip-contact-list-icon">
                            <?php if ($is_new || $migrated) {
                                Icons_Manager::render_icon($item['hip_nav_selected_icon'], ['aria-hidden' => 'true']);
                            } else { ?>
                                <i class="<?php echo esc_attr($item['icon']); ?>" aria-hidden="true"></i>
                            <?php } ?>
                        </span>
                                <?php } ?>

                                <span <?php $this->print_render_attribute_string($repeater_setting_key); ?>>
                        <?php echo esc_html($item['hip_nav_list_text']); ?>
                    </span>

                                <?php if (!empty($item['hip_nav_menu_link']['url'])) { ?>
                            </a>
                        <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
            <?php }
        }
    protected function nav_social_icons_render() {
        $settings = $this->get_settings_for_display();
        $fallback_defaults = [
          'fa fa-check',
          'fa fa-times',
          'fa fa-dot-circle-o',
        ];
        $enable_social_icons = !empty($settings['enabled_menu_social_icons']) ? $settings['hip_nav_social_icon_lists'] : [];
        $nav_social_icon_list = !empty($settings['hip_nav_social_icon_lists']) ? $settings['hip_nav_social_icon_lists'] : [];
        $migration_allowed = Icons_Manager::is_migration_allowed();

        if (!empty($enable_social_icons)) { ?>
            <ul class="hip-nav-social-icons">
                <?php foreach ($nav_social_icon_list as $index => $item) {
                    $repeater_setting_key = $this->get_repeater_setting_key('hip_nav_social_icon_text', 'hip_nav_social_icon_lists', $index);
                    $this->add_render_attribute($repeater_setting_key, 'class', 'hip-nav-social-list-text');
                    $this->add_inline_editing_attributes($repeater_setting_key);
                    $id = !empty($item['hip_nav_social_icon_id']) ? $item['hip_nav_social_icon_id'] : '';
                    $link_key = 'link_' . $index;

                    if (!empty($item['hip_nav_menu_social_icon_link']['url'])) {
                        $this->add_link_attributes($link_key, $item['hip_nav_menu_social_icon_link']);
                        $link_url = $item['hip_nav_menu_social_icon_link']['url'];
                        $link_target = !empty($item['hip_nav_menu_social_icon_link']['is_external']) ? ' target="_blank"' : '';
                        $link_nofollow = !empty($item['hip_nav_menu_social_icon_link']['nofollow']) ? ' rel="nofollow"' : '';
                    }
                    ?>
                    <li class="hip-social-list menu-item" <?php if (!empty($id)) { echo 'id="' . esc_attr($id) . '"'; } ?>>
                        <?php if (!empty($link_url)) { ?>
                        <a href="<?php echo esc_url($link_url); ?>" <?php echo $link_target . $link_nofollow; ?> <?php $this->print_render_attribute_string($link_key); ?>>
                            <?php }

                            if (!isset($item['icon']) && !$migration_allowed) {
                                $item['icon'] = isset($fallback_defaults[$index]) ? $fallback_defaults[$index] : 'fa fa-check';
                            }
                            $migrated = isset($item['__fa4_migrated']['hip_nav_selected_social_icon']);
                            $is_new = !isset($item['icon']) && $migration_allowed;
                            if (!empty($item['icon']) || (!empty($item['hip_nav_selected_social_icon']['value']) && $is_new)) { ?>
                                <span class="hip-social-list-icon">
                            <?php if ($is_new || $migrated) {
                                Icons_Manager::render_icon($item['hip_nav_selected_social_icon'], ['aria-hidden' => 'true']);
                            } else { ?>
                                <i class="<?php echo esc_attr($item['icon']); ?>" aria-hidden="true"></i>
                            <?php } ?>
                        </span>
                            <?php } ?>

                            <span <?php $this->print_render_attribute_string($repeater_setting_key); ?>>
                        <?php echo esc_html($item['hip_nav_social_icon_text']); ?>
                    </span>

                            <?php if (!empty($link_url)) { ?>
                        </a>
                    <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        <?php }
    }

    protected function hip_menu_cta_button_render(){
        $settings = $this->get_settings_for_display();
        $enable_menu_cta_button = !empty($settings['enabled_menu_cta_button']) ? $settings['enabled_menu_cta_button'] : [];

        if (!empty($enable_menu_cta_button)){
            if ( empty( $settings['hip_menu_cta_button_text'] ) && empty( $settings['hip_menu_cta_button_selected_icon']['value'] ) ) {
                return;
            }
            $this->add_render_attribute( 'wrapper', 'class', 'hip-menu-button-wrapper' );

            $this->add_render_attribute( 'button', 'class', 'hip-menu-button' );
            if ( ! empty( $settings['hip_menu_cta_button_link']['url'] ) ) {
                $this->add_link_attributes( 'button', $settings['hip_menu_cta_button_link'] );
                $this->add_render_attribute( 'button', 'class', 'hip-menu-button-link' );
            } else {
                $this->add_render_attribute( 'button', 'role', 'button' );
            }
            if ( ! empty( $settings['hip_menu_cta_button_button_css_id'] ) ) {
                $this->add_render_attribute( 'button', 'id', $settings['hip_menu_cta_button_button_css_id'] );
            }
            if ( ! empty( $settings['hip_menu_hover_animation'] ) ) {
                $this->add_render_attribute( 'button', 'class', 'elementor-animation-' . $settings['hip_menu_hover_animation'] );
            }?>
            <div <?php $this->print_render_attribute_string( 'wrapper' ); ?>>
                <a <?php $this->print_render_attribute_string( 'button' ); ?>>
                    <?php $this->hip_menu_cta_render_text(); ?>
                </a>
            </div>
            <?php
        }


    }
    protected function hip_menu_cta_render_text(){
        $settings = $this->get_settings_for_display();
        $migrated = isset( $settings['__fa4_migrated']['hip_menu_cta_button_selected_icon'] );
        $is_new = empty( $settings['icon'] ) && Icons_Manager::is_migration_allowed();

        if ( ! $is_new && empty( $settings['hip_menu_cta_button_icon_align'] ) ) {
            // @todo: remove when deprecated
            // added as bc in 2.6
            //old default
            $settings['hip_menu_cta_button_icon_align'] = $this->get_settings( 'hip_menu_cta_button_icon_align' );
        }
        $this->add_render_attribute( [
          'hip-menu-cta-button-content-wrapper' => [
            'class' => 'hip-menu-button-content-wrapper',
          ],
          'hip-cta-icon-align' => [
            'class' => [
              'hip-cta-button-icon',
              'hip-cta-align-icon-' . $settings['hip_menu_cta_button_icon_align'],
            ],
          ],
          'text' => [
            'class' => 'hip-cta-button-text',
          ],
        ] );
        ?>
        <span <?php $this->print_render_attribute_string( 'hip-menu-cta-button-content-wrapper' ); ?>>
			<?php if ( ! empty( $settings['icon'] ) || ! empty( $settings['hip_menu_cta_button_selected_icon']['value'] ) ) : ?>
                <span <?php $this->print_render_attribute_string( 'hip-cta-icon-align' ); ?>>
				<?php if ( $is_new || $migrated ) :
                    Icons_Manager::render_icon( $settings['hip_menu_cta_button_selected_icon'], [ 'aria-hidden' => 'true' ] );
                else : ?>
                    <i class="<?php echo esc_attr( $settings['icon'] ); ?>" aria-hidden="true"></i>
                <?php endif; ?>
			</span>
            <?php endif; ?>
            <?php if ( ! empty( $settings['hip_menu_cta_button_text'] ) ) : ?>
                <span <?php $this->print_render_attribute_string( 'hip_menu_cta_button_text' ); ?>><?php $this->print_unescaped_setting( 'hip_menu_cta_button_text' ); ?></span>
            <?php endif; ?>
		</span>
<?php

    }

}

Plugin::instance()->widgets_manager->register(new Hip_Nav());
